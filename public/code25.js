gdjs.RAM04Code = {};
gdjs.RAM04Code.GDNewObjectObjects1= [];
gdjs.RAM04Code.GDNewObjectObjects2= [];
gdjs.RAM04Code.GDNewObject2Objects1= [];
gdjs.RAM04Code.GDNewObject2Objects2= [];
gdjs.RAM04Code.GDRETOURObjects1= [];
gdjs.RAM04Code.GDRETOURObjects2= [];

gdjs.RAM04Code.conditionTrue_0 = {val:false};
gdjs.RAM04Code.condition0IsTrue_0 = {val:false};
gdjs.RAM04Code.condition1IsTrue_0 = {val:false};
gdjs.RAM04Code.condition2IsTrue_0 = {val:false};


gdjs.RAM04Code.mapOfGDgdjs_46RAM04Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.RAM04Code.GDRETOURObjects1});gdjs.RAM04Code.eventsList0x95218c = function(runtimeScene) {

{


gdjs.RAM04Code.condition0IsTrue_0.val = false;
if (gdjs.RAM04Code.condition0IsTrue_0.val) {
{}}

}


}; //End of gdjs.RAM04Code.eventsList0x95218c
gdjs.RAM04Code.mapOfGDgdjs_46RAM04Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.RAM04Code.GDNewObject2Objects1});gdjs.RAM04Code.eventsList0x965c9c = function(runtimeScene) {

{


gdjs.RAM04Code.condition0IsTrue_0.val = false;
if (gdjs.RAM04Code.condition0IsTrue_0.val) {
{}}

}


}; //End of gdjs.RAM04Code.eventsList0x965c9c
gdjs.RAM04Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.RAM04Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.RAM04Code.condition0IsTrue_0.val = false;
gdjs.RAM04Code.condition1IsTrue_0.val = false;
{
gdjs.RAM04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.RAM04Code.condition0IsTrue_0.val ) {
{
gdjs.RAM04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RAM04Code.mapOfGDgdjs_46RAM04Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.RAM04Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "RAM03", true);
}
{ //Subevents
gdjs.RAM04Code.eventsList0x95218c(runtimeScene);} //End of subevents
}

}


{

gdjs.RAM04Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.RAM04Code.condition0IsTrue_0.val = false;
gdjs.RAM04Code.condition1IsTrue_0.val = false;
{
gdjs.RAM04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.RAM04Code.condition0IsTrue_0.val ) {
{
gdjs.RAM04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RAM04Code.mapOfGDgdjs_46RAM04Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.RAM04Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "SF-yehaw.mp3", false, 100, 1);
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T1");
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "T1");
}
{ //Subevents
gdjs.RAM04Code.eventsList0x965c9c(runtimeScene);} //End of subevents
}

}


{


gdjs.RAM04Code.condition0IsTrue_0.val = false;
{
gdjs.RAM04Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.RAM04Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "T1");
}}

}


{


gdjs.RAM04Code.condition0IsTrue_0.val = false;
{
gdjs.RAM04Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T1");
}if (gdjs.RAM04Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "RAM05", false);
}}

}


}; //End of gdjs.RAM04Code.eventsList0x5b7028


gdjs.RAM04Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.RAM04Code.GDNewObjectObjects1.length = 0;
gdjs.RAM04Code.GDNewObjectObjects2.length = 0;
gdjs.RAM04Code.GDNewObject2Objects1.length = 0;
gdjs.RAM04Code.GDNewObject2Objects2.length = 0;
gdjs.RAM04Code.GDRETOURObjects1.length = 0;
gdjs.RAM04Code.GDRETOURObjects2.length = 0;

gdjs.RAM04Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['RAM04Code'] = gdjs.RAM04Code;
