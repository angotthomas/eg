gdjs.PROC3Code = {};
gdjs.PROC3Code.GDNewObjectObjects1= [];
gdjs.PROC3Code.GDNewObjectObjects2= [];
gdjs.PROC3Code.GDNewObject2Objects1= [];
gdjs.PROC3Code.GDNewObject2Objects2= [];
gdjs.PROC3Code.GDNewObject3Objects1= [];
gdjs.PROC3Code.GDNewObject3Objects2= [];

gdjs.PROC3Code.conditionTrue_0 = {val:false};
gdjs.PROC3Code.condition0IsTrue_0 = {val:false};
gdjs.PROC3Code.condition1IsTrue_0 = {val:false};
gdjs.PROC3Code.condition2IsTrue_0 = {val:false};


gdjs.PROC3Code.mapOfGDgdjs_46PROC3Code_46GDNewObjectObjects1Objects = Hashtable.newFrom({"NewObject": gdjs.PROC3Code.GDNewObjectObjects1});gdjs.PROC3Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.PROC3Code.GDNewObjectObjects1.createFrom(runtimeScene.getObjects("NewObject"));

gdjs.PROC3Code.condition0IsTrue_0.val = false;
gdjs.PROC3Code.condition1IsTrue_0.val = false;
{
gdjs.PROC3Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.PROC3Code.mapOfGDgdjs_46PROC3Code_46GDNewObjectObjects1Objects, runtimeScene, true, false);
}if ( gdjs.PROC3Code.condition0IsTrue_0.val ) {
{
gdjs.PROC3Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}}
if (gdjs.PROC3Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


}; //End of gdjs.PROC3Code.eventsList0x5b7028


gdjs.PROC3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.PROC3Code.GDNewObjectObjects1.length = 0;
gdjs.PROC3Code.GDNewObjectObjects2.length = 0;
gdjs.PROC3Code.GDNewObject2Objects1.length = 0;
gdjs.PROC3Code.GDNewObject2Objects2.length = 0;
gdjs.PROC3Code.GDNewObject3Objects1.length = 0;
gdjs.PROC3Code.GDNewObject3Objects2.length = 0;

gdjs.PROC3Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['PROC3Code'] = gdjs.PROC3Code;
