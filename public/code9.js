gdjs.FICHIER09Code = {};
gdjs.FICHIER09Code.GDNewObjectObjects1= [];
gdjs.FICHIER09Code.GDNewObjectObjects2= [];
gdjs.FICHIER09Code.GDECOLEObjects1= [];
gdjs.FICHIER09Code.GDECOLEObjects2= [];
gdjs.FICHIER09Code.GDOASISObjects1= [];
gdjs.FICHIER09Code.GDOASISObjects2= [];
gdjs.FICHIER09Code.GDRETOURObjects1= [];
gdjs.FICHIER09Code.GDRETOURObjects2= [];

gdjs.FICHIER09Code.conditionTrue_0 = {val:false};
gdjs.FICHIER09Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER09Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER09Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER09Code.GDRETOURObjects1});gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDECOLEObjects1Objects = Hashtable.newFrom({"ECOLE": gdjs.FICHIER09Code.GDECOLEObjects1});gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDOASISObjects1Objects = Hashtable.newFrom({"OASIS": gdjs.FICHIER09Code.GDOASISObjects1});gdjs.FICHIER09Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER09Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER09Code.condition0IsTrue_0.val = false;
gdjs.FICHIER09Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER09Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER09Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER09Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER09Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER07", true);
}}

}


{

gdjs.FICHIER09Code.GDECOLEObjects1.createFrom(runtimeScene.getObjects("ECOLE"));

gdjs.FICHIER09Code.condition0IsTrue_0.val = false;
gdjs.FICHIER09Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER09Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER09Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER09Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDECOLEObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER09Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER11", true);
}}

}


{

gdjs.FICHIER09Code.GDOASISObjects1.createFrom(runtimeScene.getObjects("OASIS"));

gdjs.FICHIER09Code.condition0IsTrue_0.val = false;
gdjs.FICHIER09Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER09Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER09Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER09Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER09Code.mapOfGDgdjs_46FICHIER09Code_46GDOASISObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER09Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER10", true);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER09Code.eventsList0x5b7028


gdjs.FICHIER09Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER09Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER09Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER09Code.GDECOLEObjects1.length = 0;
gdjs.FICHIER09Code.GDECOLEObjects2.length = 0;
gdjs.FICHIER09Code.GDOASISObjects1.length = 0;
gdjs.FICHIER09Code.GDOASISObjects2.length = 0;
gdjs.FICHIER09Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER09Code.GDRETOURObjects2.length = 0;

gdjs.FICHIER09Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER09Code'] = gdjs.FICHIER09Code;
