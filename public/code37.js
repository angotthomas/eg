gdjs.SORTIE5Code = {};
gdjs.SORTIE5Code.GDNewObjectObjects1= [];
gdjs.SORTIE5Code.GDNewObjectObjects2= [];
gdjs.SORTIE5Code.GDCHRONOObjects1= [];
gdjs.SORTIE5Code.GDCHRONOObjects2= [];

gdjs.SORTIE5Code.conditionTrue_0 = {val:false};
gdjs.SORTIE5Code.condition0IsTrue_0 = {val:false};
gdjs.SORTIE5Code.condition1IsTrue_0 = {val:false};
gdjs.SORTIE5Code.condition2IsTrue_0 = {val:false};
gdjs.SORTIE5Code.conditionTrue_1 = {val:false};
gdjs.SORTIE5Code.condition0IsTrue_1 = {val:false};
gdjs.SORTIE5Code.condition1IsTrue_1 = {val:false};
gdjs.SORTIE5Code.condition2IsTrue_1 = {val:false};


gdjs.SORTIE5Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
{gdjs.SORTIE5Code.conditionTrue_1 = gdjs.SORTIE5Code.condition0IsTrue_0;
gdjs.SORTIE5Code.condition0IsTrue_1.val = false;
gdjs.SORTIE5Code.condition1IsTrue_1.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "T");
if( gdjs.SORTIE5Code.condition0IsTrue_1.val ) {
    gdjs.SORTIE5Code.conditionTrue_1.val = true;
}
}
{
gdjs.SORTIE5Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "t");
if( gdjs.SORTIE5Code.condition1IsTrue_1.val ) {
    gdjs.SORTIE5Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIE6", false);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "bipoui.wav", false, 100, 1);
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T2");
}{gdjs.evtTools.sound.playSound(runtimeScene, "chrono.wav", true, 100, 1);
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("9");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("8");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 2, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("7");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("6");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 4, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("5");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 5, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("4");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 6, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("3");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 7, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("2");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 8, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("1");
}
}}

}


{


gdjs.SORTIE5Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE5Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 9, "T2");
}if (gdjs.SORTIE5Code.condition0IsTrue_0.val) {
gdjs.SORTIE5Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE5Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE5Code.GDCHRONOObjects1[i].setString("0");
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIEP", false);
}}

}


{


{
}

}


}; //End of gdjs.SORTIE5Code.eventsList0x5b7028


gdjs.SORTIE5Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SORTIE5Code.GDNewObjectObjects1.length = 0;
gdjs.SORTIE5Code.GDNewObjectObjects2.length = 0;
gdjs.SORTIE5Code.GDCHRONOObjects1.length = 0;
gdjs.SORTIE5Code.GDCHRONOObjects2.length = 0;

gdjs.SORTIE5Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SORTIE5Code'] = gdjs.SORTIE5Code;
