gdjs.FICHIER01Code = {};
gdjs.FICHIER01Code.GDNewObjectObjects1= [];
gdjs.FICHIER01Code.GDNewObjectObjects2= [];
gdjs.FICHIER01Code.GDSECRETObjects1= [];
gdjs.FICHIER01Code.GDSECRETObjects2= [];
gdjs.FICHIER01Code.GDDOCObjects1= [];
gdjs.FICHIER01Code.GDDOCObjects2= [];
gdjs.FICHIER01Code.GDIMPORTANTObjects1= [];
gdjs.FICHIER01Code.GDIMPORTANTObjects2= [];
gdjs.FICHIER01Code.GDRETOURObjects1= [];
gdjs.FICHIER01Code.GDRETOURObjects2= [];

gdjs.FICHIER01Code.conditionTrue_0 = {val:false};
gdjs.FICHIER01Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER01Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER01Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER01Code.GDRETOURObjects1});gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDSECRETObjects1Objects = Hashtable.newFrom({"SECRET": gdjs.FICHIER01Code.GDSECRETObjects1});gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDDOCObjects1Objects = Hashtable.newFrom({"DOC": gdjs.FICHIER01Code.GDDOCObjects1});gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDIMPORTANTObjects1Objects = Hashtable.newFrom({"IMPORTANT": gdjs.FICHIER01Code.GDIMPORTANTObjects1});gdjs.FICHIER01Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER01Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER01Code.condition0IsTrue_0.val = false;
gdjs.FICHIER01Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER01Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER01Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER01Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER01Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", true);
}}

}


{

gdjs.FICHIER01Code.GDSECRETObjects1.createFrom(runtimeScene.getObjects("SECRET"));

gdjs.FICHIER01Code.condition0IsTrue_0.val = false;
gdjs.FICHIER01Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER01Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER01Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER01Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDSECRETObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER01Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER02", true);
}}

}


{

gdjs.FICHIER01Code.GDDOCObjects1.createFrom(runtimeScene.getObjects("DOC"));

gdjs.FICHIER01Code.condition0IsTrue_0.val = false;
gdjs.FICHIER01Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER01Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER01Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER01Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDDOCObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER01Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER07", true);
}}

}


{

gdjs.FICHIER01Code.GDIMPORTANTObjects1.createFrom(runtimeScene.getObjects("IMPORTANT"));

gdjs.FICHIER01Code.condition0IsTrue_0.val = false;
gdjs.FICHIER01Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER01Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER01Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER01Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER01Code.mapOfGDgdjs_46FICHIER01Code_46GDIMPORTANTObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER01Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER13", true);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER01Code.eventsList0x5b7028


gdjs.FICHIER01Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER01Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER01Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER01Code.GDSECRETObjects1.length = 0;
gdjs.FICHIER01Code.GDSECRETObjects2.length = 0;
gdjs.FICHIER01Code.GDDOCObjects1.length = 0;
gdjs.FICHIER01Code.GDDOCObjects2.length = 0;
gdjs.FICHIER01Code.GDIMPORTANTObjects1.length = 0;
gdjs.FICHIER01Code.GDIMPORTANTObjects2.length = 0;
gdjs.FICHIER01Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER01Code.GDRETOURObjects2.length = 0;

gdjs.FICHIER01Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER01Code'] = gdjs.FICHIER01Code;
