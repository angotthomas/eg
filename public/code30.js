gdjs.PROC2Code = {};
gdjs.PROC2Code.GDNewObjectObjects1= [];
gdjs.PROC2Code.GDNewObjectObjects2= [];
gdjs.PROC2Code.GDOUIObjects1= [];
gdjs.PROC2Code.GDOUIObjects2= [];
gdjs.PROC2Code.GDNONObjects1= [];
gdjs.PROC2Code.GDNONObjects2= [];
gdjs.PROC2Code.GDDEBUTObjects1= [];
gdjs.PROC2Code.GDDEBUTObjects2= [];
gdjs.PROC2Code.GDFINObjects1= [];
gdjs.PROC2Code.GDFINObjects2= [];
gdjs.PROC2Code.GDTESTObjects1= [];
gdjs.PROC2Code.GDTESTObjects2= [];
gdjs.PROC2Code.GDLANCEObjects1= [];
gdjs.PROC2Code.GDLANCEObjects2= [];
gdjs.PROC2Code.GDOUVREObjects1= [];
gdjs.PROC2Code.GDOUVREObjects2= [];
gdjs.PROC2Code.GDCIBLEDEBUTObjects1= [];
gdjs.PROC2Code.GDCIBLEDEBUTObjects2= [];
gdjs.PROC2Code.GDCIBLETESTObjects1= [];
gdjs.PROC2Code.GDCIBLETESTObjects2= [];
gdjs.PROC2Code.GDCIBLENONObjects1= [];
gdjs.PROC2Code.GDCIBLENONObjects2= [];
gdjs.PROC2Code.GDCIBLEOUIObjects1= [];
gdjs.PROC2Code.GDCIBLEOUIObjects2= [];
gdjs.PROC2Code.GDCIBLELANCEObjects1= [];
gdjs.PROC2Code.GDCIBLELANCEObjects2= [];
gdjs.PROC2Code.GDCIBLEOUVREObjects1= [];
gdjs.PROC2Code.GDCIBLEOUVREObjects2= [];
gdjs.PROC2Code.GDCIBLEFINObjects1= [];
gdjs.PROC2Code.GDCIBLEFINObjects2= [];
gdjs.PROC2Code.GDPOSITIONDEBUTObjects1= [];
gdjs.PROC2Code.GDPOSITIONDEBUTObjects2= [];
gdjs.PROC2Code.GDPOSITIONTESTEObjects1= [];
gdjs.PROC2Code.GDPOSITIONTESTEObjects2= [];
gdjs.PROC2Code.GDPOSITIONLANCEObjects1= [];
gdjs.PROC2Code.GDPOSITIONLANCEObjects2= [];
gdjs.PROC2Code.GDPOSITIONOUVREObjects1= [];
gdjs.PROC2Code.GDPOSITIONOUVREObjects2= [];
gdjs.PROC2Code.GDPOSITIONOUIObjects1= [];
gdjs.PROC2Code.GDPOSITIONOUIObjects2= [];
gdjs.PROC2Code.GDPOSITIONNONObjects1= [];
gdjs.PROC2Code.GDPOSITIONNONObjects2= [];
gdjs.PROC2Code.GDPOSITIONFINObjects1= [];
gdjs.PROC2Code.GDPOSITIONFINObjects2= [];
gdjs.PROC2Code.GDCONSIGNESObjects1= [];
gdjs.PROC2Code.GDCONSIGNESObjects2= [];
gdjs.PROC2Code.GDRETOURObjects1= [];
gdjs.PROC2Code.GDRETOURObjects2= [];

gdjs.PROC2Code.conditionTrue_0 = {val:false};
gdjs.PROC2Code.condition0IsTrue_0 = {val:false};
gdjs.PROC2Code.condition1IsTrue_0 = {val:false};
gdjs.PROC2Code.condition2IsTrue_0 = {val:false};
gdjs.PROC2Code.condition3IsTrue_0 = {val:false};
gdjs.PROC2Code.condition4IsTrue_0 = {val:false};
gdjs.PROC2Code.condition5IsTrue_0 = {val:false};
gdjs.PROC2Code.condition6IsTrue_0 = {val:false};
gdjs.PROC2Code.condition7IsTrue_0 = {val:false};
gdjs.PROC2Code.condition8IsTrue_0 = {val:false};
gdjs.PROC2Code.conditionTrue_1 = {val:false};
gdjs.PROC2Code.condition0IsTrue_1 = {val:false};
gdjs.PROC2Code.condition1IsTrue_1 = {val:false};
gdjs.PROC2Code.condition2IsTrue_1 = {val:false};
gdjs.PROC2Code.condition3IsTrue_1 = {val:false};
gdjs.PROC2Code.condition4IsTrue_1 = {val:false};
gdjs.PROC2Code.condition5IsTrue_1 = {val:false};
gdjs.PROC2Code.condition6IsTrue_1 = {val:false};
gdjs.PROC2Code.condition7IsTrue_1 = {val:false};
gdjs.PROC2Code.condition8IsTrue_1 = {val:false};


gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDDEBUTObjects1Objects = Hashtable.newFrom({"DEBUT": gdjs.PROC2Code.GDDEBUTObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEDEBUTObjects1Objects = Hashtable.newFrom({"CIBLEDEBUT": gdjs.PROC2Code.GDCIBLEDEBUTObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDFINObjects1Objects = Hashtable.newFrom({"FIN": gdjs.PROC2Code.GDFINObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEFINObjects1Objects = Hashtable.newFrom({"CIBLEFIN": gdjs.PROC2Code.GDCIBLEFINObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDOUIObjects1Objects = Hashtable.newFrom({"OUI": gdjs.PROC2Code.GDOUIObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEOUIObjects1Objects = Hashtable.newFrom({"CIBLEOUI": gdjs.PROC2Code.GDCIBLEOUIObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDNONObjects1Objects = Hashtable.newFrom({"NON": gdjs.PROC2Code.GDNONObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLENONObjects1Objects = Hashtable.newFrom({"CIBLENON": gdjs.PROC2Code.GDCIBLENONObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDTESTObjects1Objects = Hashtable.newFrom({"TEST": gdjs.PROC2Code.GDTESTObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLETESTObjects1Objects = Hashtable.newFrom({"CIBLETEST": gdjs.PROC2Code.GDCIBLETESTObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDOUVREObjects1Objects = Hashtable.newFrom({"OUVRE": gdjs.PROC2Code.GDOUVREObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEOUVREObjects1Objects = Hashtable.newFrom({"CIBLEOUVRE": gdjs.PROC2Code.GDCIBLEOUVREObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDLANCEObjects1Objects = Hashtable.newFrom({"LANCE": gdjs.PROC2Code.GDLANCEObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLELANCEObjects1Objects = Hashtable.newFrom({"CIBLELANCE": gdjs.PROC2Code.GDCIBLELANCEObjects1});gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.PROC2Code.GDRETOURObjects1});gdjs.PROC2Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.PROC2Code.GDCIBLEDEBUTObjects1.createFrom(runtimeScene.getObjects("CIBLEDEBUT"));
gdjs.PROC2Code.GDDEBUTObjects1.createFrom(runtimeScene.getObjects("DEBUT"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDDEBUTObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEDEBUTObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDDEBUTObjects1 */
gdjs.PROC2Code.GDPOSITIONDEBUTObjects1.createFrom(runtimeScene.getObjects("POSITIONDEBUT"));
{for(var i = 0, len = gdjs.PROC2Code.GDDEBUTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDDEBUTObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDDEBUTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDDEBUTObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONDEBUTObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONDEBUTObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONDEBUTObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONDEBUTObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDDEBUTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDDEBUTObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLEFINObjects1.createFrom(runtimeScene.getObjects("CIBLEFIN"));
gdjs.PROC2Code.GDFINObjects1.createFrom(runtimeScene.getObjects("FIN"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDFINObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEFINObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDFINObjects1 */
gdjs.PROC2Code.GDPOSITIONFINObjects1.createFrom(runtimeScene.getObjects("POSITIONFIN"));
{for(var i = 0, len = gdjs.PROC2Code.GDFINObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDFINObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDFINObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDFINObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONFINObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONFINObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONFINObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONFINObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDFINObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDFINObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLEOUIObjects1.createFrom(runtimeScene.getObjects("CIBLEOUI"));
gdjs.PROC2Code.GDOUIObjects1.createFrom(runtimeScene.getObjects("OUI"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDOUIObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEOUIObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDOUIObjects1 */
gdjs.PROC2Code.GDPOSITIONOUIObjects1.createFrom(runtimeScene.getObjects("POSITIONOUI"));
{for(var i = 0, len = gdjs.PROC2Code.GDOUIObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUIObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDOUIObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUIObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONOUIObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONOUIObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONOUIObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONOUIObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDOUIObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUIObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLENONObjects1.createFrom(runtimeScene.getObjects("CIBLENON"));
gdjs.PROC2Code.GDNONObjects1.createFrom(runtimeScene.getObjects("NON"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDNONObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLENONObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDNONObjects1 */
gdjs.PROC2Code.GDPOSITIONNONObjects1.createFrom(runtimeScene.getObjects("POSITIONNON"));
{for(var i = 0, len = gdjs.PROC2Code.GDNONObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDNONObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDNONObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDNONObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONNONObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONNONObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONNONObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONNONObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDNONObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDNONObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLETESTObjects1.createFrom(runtimeScene.getObjects("CIBLETEST"));
gdjs.PROC2Code.GDTESTObjects1.createFrom(runtimeScene.getObjects("TEST"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDTESTObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLETESTObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
gdjs.PROC2Code.GDPOSITIONTESTEObjects1.createFrom(runtimeScene.getObjects("POSITIONTESTE"));
/* Reuse gdjs.PROC2Code.GDTESTObjects1 */
{for(var i = 0, len = gdjs.PROC2Code.GDTESTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDTESTObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDTESTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDTESTObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONTESTEObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONTESTEObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONTESTEObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONTESTEObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDTESTObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDTESTObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLEOUVREObjects1.createFrom(runtimeScene.getObjects("CIBLEOUVRE"));
gdjs.PROC2Code.GDOUVREObjects1.createFrom(runtimeScene.getObjects("OUVRE"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDOUVREObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLEOUVREObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDOUVREObjects1 */
gdjs.PROC2Code.GDPOSITIONOUVREObjects1.createFrom(runtimeScene.getObjects("POSITIONOUVRE"));
{for(var i = 0, len = gdjs.PROC2Code.GDOUVREObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUVREObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDOUVREObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUVREObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONOUVREObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONOUVREObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONOUVREObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONOUVREObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDOUVREObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDOUVREObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDCIBLELANCEObjects1.createFrom(runtimeScene.getObjects("CIBLELANCE"));
gdjs.PROC2Code.GDLANCEObjects1.createFrom(runtimeScene.getObjects("LANCE"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.object.hitBoxesCollisionTest(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDLANCEObjects1Objects, gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDCIBLELANCEObjects1Objects, false, runtimeScene, false);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
/* Reuse gdjs.PROC2Code.GDLANCEObjects1 */
gdjs.PROC2Code.GDPOSITIONLANCEObjects1.createFrom(runtimeScene.getObjects("POSITIONLANCE"));
{for(var i = 0, len = gdjs.PROC2Code.GDLANCEObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDLANCEObjects1[i].activateBehavior("Déplaçable", false);
}
}{for(var i = 0, len = gdjs.PROC2Code.GDLANCEObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDLANCEObjects1[i].setPosition((( gdjs.PROC2Code.GDPOSITIONLANCEObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONLANCEObjects1[0].getPointX("")),(( gdjs.PROC2Code.GDPOSITIONLANCEObjects1.length === 0 ) ? 0 :gdjs.PROC2Code.GDPOSITIONLANCEObjects1[0].getPointY("")));
}
}{for(var i = 0, len = gdjs.PROC2Code.GDLANCEObjects1.length ;i < len;++i) {
    gdjs.PROC2Code.GDLANCEObjects1[i].setColor("126;211;33");
}
}}

}


{

gdjs.PROC2Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
gdjs.PROC2Code.condition1IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.PROC2Code.condition0IsTrue_0.val ) {
{
gdjs.PROC2Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.PROC2Code.mapOfGDgdjs_46PROC2Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.PROC2Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


{

gdjs.PROC2Code.GDDEBUTObjects1.createFrom(runtimeScene.getObjects("DEBUT"));
gdjs.PROC2Code.GDFINObjects1.createFrom(runtimeScene.getObjects("FIN"));
gdjs.PROC2Code.GDLANCEObjects1.createFrom(runtimeScene.getObjects("LANCE"));
gdjs.PROC2Code.GDNONObjects1.createFrom(runtimeScene.getObjects("NON"));
gdjs.PROC2Code.GDOUIObjects1.createFrom(runtimeScene.getObjects("OUI"));
gdjs.PROC2Code.GDOUVREObjects1.createFrom(runtimeScene.getObjects("OUVRE"));
gdjs.PROC2Code.GDTESTObjects1.createFrom(runtimeScene.getObjects("TEST"));

gdjs.PROC2Code.condition0IsTrue_0.val = false;
gdjs.PROC2Code.condition1IsTrue_0.val = false;
gdjs.PROC2Code.condition2IsTrue_0.val = false;
gdjs.PROC2Code.condition3IsTrue_0.val = false;
gdjs.PROC2Code.condition4IsTrue_0.val = false;
gdjs.PROC2Code.condition5IsTrue_0.val = false;
gdjs.PROC2Code.condition6IsTrue_0.val = false;
gdjs.PROC2Code.condition7IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDOUIObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDOUIObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition0IsTrue_0.val = true;
        gdjs.PROC2Code.GDOUIObjects1[k] = gdjs.PROC2Code.GDOUIObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDOUIObjects1.length = k;}if ( gdjs.PROC2Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDNONObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDNONObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition1IsTrue_0.val = true;
        gdjs.PROC2Code.GDNONObjects1[k] = gdjs.PROC2Code.GDNONObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDNONObjects1.length = k;}if ( gdjs.PROC2Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDDEBUTObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDDEBUTObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition2IsTrue_0.val = true;
        gdjs.PROC2Code.GDDEBUTObjects1[k] = gdjs.PROC2Code.GDDEBUTObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDDEBUTObjects1.length = k;}if ( gdjs.PROC2Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDFINObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDFINObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition3IsTrue_0.val = true;
        gdjs.PROC2Code.GDFINObjects1[k] = gdjs.PROC2Code.GDFINObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDFINObjects1.length = k;}if ( gdjs.PROC2Code.condition3IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDLANCEObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDLANCEObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition4IsTrue_0.val = true;
        gdjs.PROC2Code.GDLANCEObjects1[k] = gdjs.PROC2Code.GDLANCEObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDLANCEObjects1.length = k;}if ( gdjs.PROC2Code.condition4IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDOUVREObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDOUVREObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition5IsTrue_0.val = true;
        gdjs.PROC2Code.GDOUVREObjects1[k] = gdjs.PROC2Code.GDOUVREObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDOUVREObjects1.length = k;}if ( gdjs.PROC2Code.condition5IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.PROC2Code.GDTESTObjects1.length;i<l;++i) {
    if ( !(gdjs.PROC2Code.GDTESTObjects1[i].behaviorActivated("Déplaçable")) ) {
        gdjs.PROC2Code.condition6IsTrue_0.val = true;
        gdjs.PROC2Code.GDTESTObjects1[k] = gdjs.PROC2Code.GDTESTObjects1[i];
        ++k;
    }
}
gdjs.PROC2Code.GDTESTObjects1.length = k;}if ( gdjs.PROC2Code.condition6IsTrue_0.val ) {
{
{gdjs.PROC2Code.conditionTrue_1 = gdjs.PROC2Code.condition7IsTrue_0;
gdjs.PROC2Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9921564);
}
}}
}
}
}
}
}
}
if (gdjs.PROC2Code.condition7IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "SF-yehaw.mp3", false, 100, 1);
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "t1");
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "t1");
}{runtimeScene.getGame().getVariables().getFromIndex(0).setNumber(1);
}}

}


{


gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "t1");
}}

}


{


gdjs.PROC2Code.condition0IsTrue_0.val = false;
{
gdjs.PROC2Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "t1");
}if (gdjs.PROC2Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "PROC3", false);
}}

}


}; //End of gdjs.PROC2Code.eventsList0x5b7028


gdjs.PROC2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.PROC2Code.GDNewObjectObjects1.length = 0;
gdjs.PROC2Code.GDNewObjectObjects2.length = 0;
gdjs.PROC2Code.GDOUIObjects1.length = 0;
gdjs.PROC2Code.GDOUIObjects2.length = 0;
gdjs.PROC2Code.GDNONObjects1.length = 0;
gdjs.PROC2Code.GDNONObjects2.length = 0;
gdjs.PROC2Code.GDDEBUTObjects1.length = 0;
gdjs.PROC2Code.GDDEBUTObjects2.length = 0;
gdjs.PROC2Code.GDFINObjects1.length = 0;
gdjs.PROC2Code.GDFINObjects2.length = 0;
gdjs.PROC2Code.GDTESTObjects1.length = 0;
gdjs.PROC2Code.GDTESTObjects2.length = 0;
gdjs.PROC2Code.GDLANCEObjects1.length = 0;
gdjs.PROC2Code.GDLANCEObjects2.length = 0;
gdjs.PROC2Code.GDOUVREObjects1.length = 0;
gdjs.PROC2Code.GDOUVREObjects2.length = 0;
gdjs.PROC2Code.GDCIBLEDEBUTObjects1.length = 0;
gdjs.PROC2Code.GDCIBLEDEBUTObjects2.length = 0;
gdjs.PROC2Code.GDCIBLETESTObjects1.length = 0;
gdjs.PROC2Code.GDCIBLETESTObjects2.length = 0;
gdjs.PROC2Code.GDCIBLENONObjects1.length = 0;
gdjs.PROC2Code.GDCIBLENONObjects2.length = 0;
gdjs.PROC2Code.GDCIBLEOUIObjects1.length = 0;
gdjs.PROC2Code.GDCIBLEOUIObjects2.length = 0;
gdjs.PROC2Code.GDCIBLELANCEObjects1.length = 0;
gdjs.PROC2Code.GDCIBLELANCEObjects2.length = 0;
gdjs.PROC2Code.GDCIBLEOUVREObjects1.length = 0;
gdjs.PROC2Code.GDCIBLEOUVREObjects2.length = 0;
gdjs.PROC2Code.GDCIBLEFINObjects1.length = 0;
gdjs.PROC2Code.GDCIBLEFINObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONDEBUTObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONDEBUTObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONTESTEObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONTESTEObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONLANCEObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONLANCEObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONOUVREObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONOUVREObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONOUIObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONOUIObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONNONObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONNONObjects2.length = 0;
gdjs.PROC2Code.GDPOSITIONFINObjects1.length = 0;
gdjs.PROC2Code.GDPOSITIONFINObjects2.length = 0;
gdjs.PROC2Code.GDCONSIGNESObjects1.length = 0;
gdjs.PROC2Code.GDCONSIGNESObjects2.length = 0;
gdjs.PROC2Code.GDRETOURObjects1.length = 0;
gdjs.PROC2Code.GDRETOURObjects2.length = 0;

gdjs.PROC2Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['PROC2Code'] = gdjs.PROC2Code;
