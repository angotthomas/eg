gdjs.FICHIER02Code = {};
gdjs.FICHIER02Code.GDNewObjectObjects1= [];
gdjs.FICHIER02Code.GDNewObjectObjects2= [];
gdjs.FICHIER02Code.GDRETOURObjects1= [];
gdjs.FICHIER02Code.GDRETOURObjects2= [];
gdjs.FICHIER02Code.GDTOPSECRETObjects1= [];
gdjs.FICHIER02Code.GDTOPSECRETObjects2= [];
gdjs.FICHIER02Code.GDNEPASOUVRIRObjects1= [];
gdjs.FICHIER02Code.GDNEPASOUVRIRObjects2= [];

gdjs.FICHIER02Code.conditionTrue_0 = {val:false};
gdjs.FICHIER02Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER02Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER02Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER02Code.GDRETOURObjects1});gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDTOPSECRETObjects1Objects = Hashtable.newFrom({"TOPSECRET": gdjs.FICHIER02Code.GDTOPSECRETObjects1});gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDNEPASOUVRIRObjects1Objects = Hashtable.newFrom({"NEPASOUVRIR": gdjs.FICHIER02Code.GDNEPASOUVRIRObjects1});gdjs.FICHIER02Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER02Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER02Code.condition0IsTrue_0.val = false;
gdjs.FICHIER02Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER02Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER02Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER02Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER01", true);
}}

}


{

gdjs.FICHIER02Code.GDTOPSECRETObjects1.createFrom(runtimeScene.getObjects("TOPSECRET"));

gdjs.FICHIER02Code.condition0IsTrue_0.val = false;
gdjs.FICHIER02Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER02Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER02Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDTOPSECRETObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER02Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER04", true);
}}

}


{

gdjs.FICHIER02Code.GDNEPASOUVRIRObjects1.createFrom(runtimeScene.getObjects("NEPASOUVRIR"));

gdjs.FICHIER02Code.condition0IsTrue_0.val = false;
gdjs.FICHIER02Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER02Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER02Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER02Code.mapOfGDgdjs_46FICHIER02Code_46GDNEPASOUVRIRObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER02Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER03", true);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER02Code.eventsList0x5b7028


gdjs.FICHIER02Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER02Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER02Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER02Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER02Code.GDRETOURObjects2.length = 0;
gdjs.FICHIER02Code.GDTOPSECRETObjects1.length = 0;
gdjs.FICHIER02Code.GDTOPSECRETObjects2.length = 0;
gdjs.FICHIER02Code.GDNEPASOUVRIRObjects1.length = 0;
gdjs.FICHIER02Code.GDNEPASOUVRIRObjects2.length = 0;

gdjs.FICHIER02Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER02Code'] = gdjs.FICHIER02Code;
