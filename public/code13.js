gdjs.FICHIER13Code = {};
gdjs.FICHIER13Code.GDNewObjectObjects1= [];
gdjs.FICHIER13Code.GDNewObjectObjects2= [];
gdjs.FICHIER13Code.GDKIKIObjects1= [];
gdjs.FICHIER13Code.GDKIKIObjects2= [];
gdjs.FICHIER13Code.GDRAPPORTObjects1= [];
gdjs.FICHIER13Code.GDRAPPORTObjects2= [];
gdjs.FICHIER13Code.GDCUISINEObjects1= [];
gdjs.FICHIER13Code.GDCUISINEObjects2= [];
gdjs.FICHIER13Code.GDRETOURObjects1= [];
gdjs.FICHIER13Code.GDRETOURObjects2= [];

gdjs.FICHIER13Code.conditionTrue_0 = {val:false};
gdjs.FICHIER13Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER13Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER13Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER13Code.GDRETOURObjects1});gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDRAPPORTObjects1Objects = Hashtable.newFrom({"RAPPORT": gdjs.FICHIER13Code.GDRAPPORTObjects1});gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDCUISINEObjects1Objects = Hashtable.newFrom({"CUISINE": gdjs.FICHIER13Code.GDCUISINEObjects1});gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDKIKIObjects1Objects = Hashtable.newFrom({"KIKI": gdjs.FICHIER13Code.GDKIKIObjects1});gdjs.FICHIER13Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER13Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER13Code.condition0IsTrue_0.val = false;
gdjs.FICHIER13Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER13Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER13Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER13Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER01", true);
}}

}


{

gdjs.FICHIER13Code.GDRAPPORTObjects1.createFrom(runtimeScene.getObjects("RAPPORT"));

gdjs.FICHIER13Code.condition0IsTrue_0.val = false;
gdjs.FICHIER13Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER13Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER13Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDRAPPORTObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER13Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER14", true);
}}

}


{

gdjs.FICHIER13Code.GDCUISINEObjects1.createFrom(runtimeScene.getObjects("CUISINE"));

gdjs.FICHIER13Code.condition0IsTrue_0.val = false;
gdjs.FICHIER13Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER13Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER13Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDCUISINEObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER13Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER15", true);
}}

}


{

gdjs.FICHIER13Code.GDKIKIObjects1.createFrom(runtimeScene.getObjects("KIKI"));

gdjs.FICHIER13Code.condition0IsTrue_0.val = false;
gdjs.FICHIER13Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER13Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER13Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER13Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER13Code.mapOfGDgdjs_46FICHIER13Code_46GDKIKIObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER13Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIERKIKI", true);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER13Code.eventsList0x5b7028


gdjs.FICHIER13Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER13Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER13Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER13Code.GDKIKIObjects1.length = 0;
gdjs.FICHIER13Code.GDKIKIObjects2.length = 0;
gdjs.FICHIER13Code.GDRAPPORTObjects1.length = 0;
gdjs.FICHIER13Code.GDRAPPORTObjects2.length = 0;
gdjs.FICHIER13Code.GDCUISINEObjects1.length = 0;
gdjs.FICHIER13Code.GDCUISINEObjects2.length = 0;
gdjs.FICHIER13Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER13Code.GDRETOURObjects2.length = 0;

gdjs.FICHIER13Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER13Code'] = gdjs.FICHIER13Code;
