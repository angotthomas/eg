gdjs.MAPCode = {};
gdjs.MAPCode.GDEXITObjects1_1final = [];

gdjs.MAPCode.GDSORTIEObjects1_1final = [];

gdjs.MAPCode.GDCARTEMObjects1= [];
gdjs.MAPCode.GDCARTEMObjects2= [];
gdjs.MAPCode.GDDISKDURObjects1= [];
gdjs.MAPCode.GDDISKDURObjects2= [];
gdjs.MAPCode.GDECRANObjects1= [];
gdjs.MAPCode.GDECRANObjects2= [];
gdjs.MAPCode.GDMEMOIREObjects1= [];
gdjs.MAPCode.GDMEMOIREObjects2= [];
gdjs.MAPCode.GDPROCObjects1= [];
gdjs.MAPCode.GDPROCObjects2= [];
gdjs.MAPCode.GDNewObjectObjects1= [];
gdjs.MAPCode.GDNewObjectObjects2= [];
gdjs.MAPCode.GDSORTIEObjects1= [];
gdjs.MAPCode.GDSORTIEObjects2= [];
gdjs.MAPCode.GDEXITObjects1= [];
gdjs.MAPCode.GDEXITObjects2= [];
gdjs.MAPCode.GDODDObjects1= [];
gdjs.MAPCode.GDODDObjects2= [];
gdjs.MAPCode.GDOMEMOIREObjects1= [];
gdjs.MAPCode.GDOMEMOIREObjects2= [];
gdjs.MAPCode.GDOCMObjects1= [];
gdjs.MAPCode.GDOCMObjects2= [];
gdjs.MAPCode.GDOECRANObjects1= [];
gdjs.MAPCode.GDOECRANObjects2= [];
gdjs.MAPCode.GDOPObjects1= [];
gdjs.MAPCode.GDOPObjects2= [];

gdjs.MAPCode.conditionTrue_0 = {val:false};
gdjs.MAPCode.condition0IsTrue_0 = {val:false};
gdjs.MAPCode.condition1IsTrue_0 = {val:false};
gdjs.MAPCode.condition2IsTrue_0 = {val:false};
gdjs.MAPCode.condition3IsTrue_0 = {val:false};
gdjs.MAPCode.conditionTrue_1 = {val:false};
gdjs.MAPCode.condition0IsTrue_1 = {val:false};
gdjs.MAPCode.condition1IsTrue_1 = {val:false};
gdjs.MAPCode.condition2IsTrue_1 = {val:false};
gdjs.MAPCode.condition3IsTrue_1 = {val:false};


gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDDISKDURObjects1Objects = Hashtable.newFrom({"DISKDUR": gdjs.MAPCode.GDDISKDURObjects1});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDMEMOIREObjects1Objects = Hashtable.newFrom({"MEMOIRE": gdjs.MAPCode.GDMEMOIREObjects1});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDCARTEMObjects1Objects = Hashtable.newFrom({"CARTEM": gdjs.MAPCode.GDCARTEMObjects1});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDPROCObjects1Objects = Hashtable.newFrom({"PROC": gdjs.MAPCode.GDPROCObjects1});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDECRANObjects1Objects = Hashtable.newFrom({"ECRAN": gdjs.MAPCode.GDECRANObjects1});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDSORTIEObjects2Objects = Hashtable.newFrom({"SORTIE": gdjs.MAPCode.GDSORTIEObjects2});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDEXITObjects2Objects = Hashtable.newFrom({"EXIT": gdjs.MAPCode.GDEXITObjects2});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDSORTIEObjects2Objects = Hashtable.newFrom({"SORTIE": gdjs.MAPCode.GDSORTIEObjects2});gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDEXITObjects2Objects = Hashtable.newFrom({"EXIT": gdjs.MAPCode.GDEXITObjects2});gdjs.MAPCode.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.MAPCode.GDDISKDURObjects1.createFrom(runtimeScene.getObjects("DISKDUR"));

gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
gdjs.MAPCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDDISKDURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MAPCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER01", true);
}}

}


{

gdjs.MAPCode.GDMEMOIREObjects1.createFrom(runtimeScene.getObjects("MEMOIRE"));

gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
gdjs.MAPCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDMEMOIREObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MAPCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "RAM01", true);
}}

}


{

gdjs.MAPCode.GDCARTEMObjects1.createFrom(runtimeScene.getObjects("CARTEM"));

gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
gdjs.MAPCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDCARTEMObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MAPCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "CM01", true);
}}

}


{

gdjs.MAPCode.GDPROCObjects1.createFrom(runtimeScene.getObjects("PROC"));

gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
gdjs.MAPCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDPROCObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MAPCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "PROC1", true);
}}

}


{

gdjs.MAPCode.GDECRANObjects1.createFrom(runtimeScene.getObjects("ECRAN"));

gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
gdjs.MAPCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDECRANObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.MAPCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MON1", true);
}}

}


{

gdjs.MAPCode.GDEXITObjects1.length = 0;

gdjs.MAPCode.GDSORTIEObjects1.length = 0;


gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
gdjs.MAPCode.condition2IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
{gdjs.MAPCode.conditionTrue_1 = gdjs.MAPCode.condition1IsTrue_0;
gdjs.MAPCode.GDEXITObjects1_1final.length = 0;gdjs.MAPCode.GDSORTIEObjects1_1final.length = 0;gdjs.MAPCode.condition0IsTrue_1.val = false;
gdjs.MAPCode.condition1IsTrue_1.val = false;
{
gdjs.MAPCode.GDSORTIEObjects2.createFrom(runtimeScene.getObjects("SORTIE"));
gdjs.MAPCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDSORTIEObjects2Objects, runtimeScene, true, false);
if( gdjs.MAPCode.condition0IsTrue_1.val ) {
    gdjs.MAPCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MAPCode.GDSORTIEObjects2.length;j<jLen;++j) {
        if ( gdjs.MAPCode.GDSORTIEObjects1_1final.indexOf(gdjs.MAPCode.GDSORTIEObjects2[j]) === -1 )
            gdjs.MAPCode.GDSORTIEObjects1_1final.push(gdjs.MAPCode.GDSORTIEObjects2[j]);
    }
}
}
{
gdjs.MAPCode.GDEXITObjects2.createFrom(runtimeScene.getObjects("EXIT"));
gdjs.MAPCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDEXITObjects2Objects, runtimeScene, true, false);
if( gdjs.MAPCode.condition1IsTrue_1.val ) {
    gdjs.MAPCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MAPCode.GDEXITObjects2.length;j<jLen;++j) {
        if ( gdjs.MAPCode.GDEXITObjects1_1final.indexOf(gdjs.MAPCode.GDEXITObjects2[j]) === -1 )
            gdjs.MAPCode.GDEXITObjects1_1final.push(gdjs.MAPCode.GDEXITObjects2[j]);
    }
}
}
{
gdjs.MAPCode.GDEXITObjects1.createFrom(gdjs.MAPCode.GDEXITObjects1_1final);
gdjs.MAPCode.GDSORTIEObjects1.createFrom(gdjs.MAPCode.GDSORTIEObjects1_1final);
}
}
}if ( gdjs.MAPCode.condition1IsTrue_0.val ) {
{
gdjs.MAPCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}}
}
if (gdjs.MAPCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIE1", true);
}}

}


{

gdjs.MAPCode.GDEXITObjects1.length = 0;

gdjs.MAPCode.GDSORTIEObjects1.length = 0;


gdjs.MAPCode.condition0IsTrue_0.val = false;
gdjs.MAPCode.condition1IsTrue_0.val = false;
gdjs.MAPCode.condition2IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.MAPCode.condition0IsTrue_0.val ) {
{
{gdjs.MAPCode.conditionTrue_1 = gdjs.MAPCode.condition1IsTrue_0;
gdjs.MAPCode.GDEXITObjects1_1final.length = 0;gdjs.MAPCode.GDSORTIEObjects1_1final.length = 0;gdjs.MAPCode.condition0IsTrue_1.val = false;
gdjs.MAPCode.condition1IsTrue_1.val = false;
{
gdjs.MAPCode.GDSORTIEObjects2.createFrom(runtimeScene.getObjects("SORTIE"));
gdjs.MAPCode.condition0IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDSORTIEObjects2Objects, runtimeScene, true, false);
if( gdjs.MAPCode.condition0IsTrue_1.val ) {
    gdjs.MAPCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MAPCode.GDSORTIEObjects2.length;j<jLen;++j) {
        if ( gdjs.MAPCode.GDSORTIEObjects1_1final.indexOf(gdjs.MAPCode.GDSORTIEObjects2[j]) === -1 )
            gdjs.MAPCode.GDSORTIEObjects1_1final.push(gdjs.MAPCode.GDSORTIEObjects2[j]);
    }
}
}
{
gdjs.MAPCode.GDEXITObjects2.createFrom(runtimeScene.getObjects("EXIT"));
gdjs.MAPCode.condition1IsTrue_1.val = gdjs.evtTools.input.cursorOnObject(gdjs.MAPCode.mapOfGDgdjs_46MAPCode_46GDEXITObjects2Objects, runtimeScene, true, false);
if( gdjs.MAPCode.condition1IsTrue_1.val ) {
    gdjs.MAPCode.conditionTrue_1.val = true;
    for(var j = 0, jLen = gdjs.MAPCode.GDEXITObjects2.length;j<jLen;++j) {
        if ( gdjs.MAPCode.GDEXITObjects1_1final.indexOf(gdjs.MAPCode.GDEXITObjects2[j]) === -1 )
            gdjs.MAPCode.GDEXITObjects1_1final.push(gdjs.MAPCode.GDEXITObjects2[j]);
    }
}
}
{
gdjs.MAPCode.GDEXITObjects1.createFrom(gdjs.MAPCode.GDEXITObjects1_1final);
gdjs.MAPCode.GDSORTIEObjects1.createFrom(gdjs.MAPCode.GDSORTIEObjects1_1final);
}
}
}if ( gdjs.MAPCode.condition1IsTrue_0.val ) {
{
gdjs.MAPCode.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 0;
}}
}
if (gdjs.MAPCode.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIEFERMEE", true);
}}

}


{


{
}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDOPObjects1.createFrom(runtimeScene.getObjects("OP"));
{for(var i = 0, len = gdjs.MAPCode.GDOPObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDOPObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(2)) == 1;
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDODDObjects1.createFrom(runtimeScene.getObjects("ODD"));
{for(var i = 0, len = gdjs.MAPCode.GDODDObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDODDObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(1)) == 1;
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDOMEMOIREObjects1.createFrom(runtimeScene.getObjects("OMEMOIRE"));
{for(var i = 0, len = gdjs.MAPCode.GDOMEMOIREObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDOMEMOIREObjects1[i].setAnimation(1);
}
}}

}


{


{
}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(3)) == 1;
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDEXITObjects1.createFrom(runtimeScene.getObjects("EXIT"));
gdjs.MAPCode.GDOECRANObjects1.createFrom(runtimeScene.getObjects("OECRAN"));
{for(var i = 0, len = gdjs.MAPCode.GDOECRANObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDOECRANObjects1[i].setAnimation(1);
}
}{for(var i = 0, len = gdjs.MAPCode.GDEXITObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDEXITObjects1[i].setAnimation(1);
}
}}

}


{


{
}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(5)) == 1;
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDOCMObjects1.createFrom(runtimeScene.getObjects("OCM"));
{for(var i = 0, len = gdjs.MAPCode.GDOCMObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDOCMObjects1[i].setAnimation(1);
}
}}

}


{


gdjs.MAPCode.condition0IsTrue_0.val = false;
{
gdjs.MAPCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MAPCode.condition0IsTrue_0.val) {
gdjs.MAPCode.GDSORTIEObjects1.createFrom(runtimeScene.getObjects("SORTIE"));
{for(var i = 0, len = gdjs.MAPCode.GDSORTIEObjects1.length ;i < len;++i) {
    gdjs.MAPCode.GDSORTIEObjects1[i].activateBehavior("Flash", true);
}
}}

}


}; //End of gdjs.MAPCode.eventsList0x5b7028


gdjs.MAPCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MAPCode.GDCARTEMObjects1.length = 0;
gdjs.MAPCode.GDCARTEMObjects2.length = 0;
gdjs.MAPCode.GDDISKDURObjects1.length = 0;
gdjs.MAPCode.GDDISKDURObjects2.length = 0;
gdjs.MAPCode.GDECRANObjects1.length = 0;
gdjs.MAPCode.GDECRANObjects2.length = 0;
gdjs.MAPCode.GDMEMOIREObjects1.length = 0;
gdjs.MAPCode.GDMEMOIREObjects2.length = 0;
gdjs.MAPCode.GDPROCObjects1.length = 0;
gdjs.MAPCode.GDPROCObjects2.length = 0;
gdjs.MAPCode.GDNewObjectObjects1.length = 0;
gdjs.MAPCode.GDNewObjectObjects2.length = 0;
gdjs.MAPCode.GDSORTIEObjects1.length = 0;
gdjs.MAPCode.GDSORTIEObjects2.length = 0;
gdjs.MAPCode.GDEXITObjects1.length = 0;
gdjs.MAPCode.GDEXITObjects2.length = 0;
gdjs.MAPCode.GDODDObjects1.length = 0;
gdjs.MAPCode.GDODDObjects2.length = 0;
gdjs.MAPCode.GDOMEMOIREObjects1.length = 0;
gdjs.MAPCode.GDOMEMOIREObjects2.length = 0;
gdjs.MAPCode.GDOCMObjects1.length = 0;
gdjs.MAPCode.GDOCMObjects2.length = 0;
gdjs.MAPCode.GDOECRANObjects1.length = 0;
gdjs.MAPCode.GDOECRANObjects2.length = 0;
gdjs.MAPCode.GDOPObjects1.length = 0;
gdjs.MAPCode.GDOPObjects2.length = 0;

gdjs.MAPCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['MAPCode'] = gdjs.MAPCode;
