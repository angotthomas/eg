gdjs.FICHIER04Code = {};
gdjs.FICHIER04Code.GDNewObjectObjects1= [];
gdjs.FICHIER04Code.GDNewObjectObjects2= [];
gdjs.FICHIER04Code.GDREPONSEObjects1= [];
gdjs.FICHIER04Code.GDREPONSEObjects2= [];
gdjs.FICHIER04Code.GDSORTIRObjects1= [];
gdjs.FICHIER04Code.GDSORTIRObjects2= [];
gdjs.FICHIER04Code.GDRETOURObjects1= [];
gdjs.FICHIER04Code.GDRETOURObjects2= [];

gdjs.FICHIER04Code.conditionTrue_0 = {val:false};
gdjs.FICHIER04Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER04Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER04Code.condition2IsTrue_0 = {val:false};
gdjs.FICHIER04Code.condition3IsTrue_0 = {val:false};


gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER04Code.GDRETOURObjects1});gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDSORTIRObjects1Objects = Hashtable.newFrom({"SORTIR": gdjs.FICHIER04Code.GDSORTIRObjects1});gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDREPONSEObjects1Objects = Hashtable.newFrom({"REPONSE": gdjs.FICHIER04Code.GDREPONSEObjects1});gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDREPONSEObjects1Objects = Hashtable.newFrom({"REPONSE": gdjs.FICHIER04Code.GDREPONSEObjects1});gdjs.FICHIER04Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER04Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER04Code.condition0IsTrue_0.val = false;
gdjs.FICHIER04Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER04Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER04Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER02", true);
}}

}


{

gdjs.FICHIER04Code.GDSORTIRObjects1.createFrom(runtimeScene.getObjects("SORTIR"));

gdjs.FICHIER04Code.condition0IsTrue_0.val = false;
gdjs.FICHIER04Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER04Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDSORTIRObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER04Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER05", true);
}}

}


{

gdjs.FICHIER04Code.GDREPONSEObjects1.createFrom(runtimeScene.getObjects("REPONSE"));

gdjs.FICHIER04Code.condition0IsTrue_0.val = false;
gdjs.FICHIER04Code.condition1IsTrue_0.val = false;
gdjs.FICHIER04Code.condition2IsTrue_0.val = false;
{
gdjs.FICHIER04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER04Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDREPONSEObjects1Objects, runtimeScene, true, false);
}if ( gdjs.FICHIER04Code.condition1IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 1;
}}
}
if (gdjs.FICHIER04Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER06", true);
}}

}


{

gdjs.FICHIER04Code.GDREPONSEObjects1.createFrom(runtimeScene.getObjects("REPONSE"));

gdjs.FICHIER04Code.condition0IsTrue_0.val = false;
gdjs.FICHIER04Code.condition1IsTrue_0.val = false;
gdjs.FICHIER04Code.condition2IsTrue_0.val = false;
{
gdjs.FICHIER04Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER04Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER04Code.mapOfGDgdjs_46FICHIER04Code_46GDREPONSEObjects1Objects, runtimeScene, true, false);
}if ( gdjs.FICHIER04Code.condition1IsTrue_0.val ) {
{
gdjs.FICHIER04Code.condition2IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getGame().getVariables().getFromIndex(0)) == 0;
}}
}
if (gdjs.FICHIER04Code.condition2IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "BUG", true);
}}

}


}; //End of gdjs.FICHIER04Code.eventsList0x5b7028


gdjs.FICHIER04Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER04Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER04Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER04Code.GDREPONSEObjects1.length = 0;
gdjs.FICHIER04Code.GDREPONSEObjects2.length = 0;
gdjs.FICHIER04Code.GDSORTIRObjects1.length = 0;
gdjs.FICHIER04Code.GDSORTIRObjects2.length = 0;
gdjs.FICHIER04Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER04Code.GDRETOURObjects2.length = 0;

gdjs.FICHIER04Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER04Code'] = gdjs.FICHIER04Code;
