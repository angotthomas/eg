gdjs.intro1Code = {};
gdjs.intro1Code.GDfondObjects1= [];
gdjs.intro1Code.GDfondObjects2= [];

gdjs.intro1Code.conditionTrue_0 = {val:false};
gdjs.intro1Code.condition0IsTrue_0 = {val:false};
gdjs.intro1Code.condition1IsTrue_0 = {val:false};
gdjs.intro1Code.condition2IsTrue_0 = {val:false};


gdjs.intro1Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.intro1Code.condition0IsTrue_0.val = false;
{
gdjs.intro1Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.intro1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "AlienSunset.mp3", true, 100, 1);
}}

}


{

gdjs.intro1Code.GDfondObjects1.createFrom(runtimeScene.getObjects("fond"));

gdjs.intro1Code.condition0IsTrue_0.val = false;
gdjs.intro1Code.condition1IsTrue_0.val = false;
{
gdjs.intro1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.intro1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.intro1Code.GDfondObjects1.length;i<l;++i) {
    if ( gdjs.intro1Code.GDfondObjects1[i].getAnimation() == 3 ) {
        gdjs.intro1Code.condition1IsTrue_0.val = true;
        gdjs.intro1Code.GDfondObjects1[k] = gdjs.intro1Code.GDfondObjects1[i];
        ++k;
    }
}
gdjs.intro1Code.GDfondObjects1.length = k;}}
if (gdjs.intro1Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", true);
}}

}


{

gdjs.intro1Code.GDfondObjects1.createFrom(runtimeScene.getObjects("fond"));

gdjs.intro1Code.condition0IsTrue_0.val = false;
gdjs.intro1Code.condition1IsTrue_0.val = false;
{
gdjs.intro1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.intro1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.intro1Code.GDfondObjects1.length;i<l;++i) {
    if ( gdjs.intro1Code.GDfondObjects1[i].getAnimation() == 2 ) {
        gdjs.intro1Code.condition1IsTrue_0.val = true;
        gdjs.intro1Code.GDfondObjects1[k] = gdjs.intro1Code.GDfondObjects1[i];
        ++k;
    }
}
gdjs.intro1Code.GDfondObjects1.length = k;}}
if (gdjs.intro1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.intro1Code.GDfondObjects1 */
{for(var i = 0, len = gdjs.intro1Code.GDfondObjects1.length ;i < len;++i) {
    gdjs.intro1Code.GDfondObjects1[i].setAnimation(3);
}
}}

}


{

gdjs.intro1Code.GDfondObjects1.createFrom(runtimeScene.getObjects("fond"));

gdjs.intro1Code.condition0IsTrue_0.val = false;
gdjs.intro1Code.condition1IsTrue_0.val = false;
{
gdjs.intro1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.intro1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.intro1Code.GDfondObjects1.length;i<l;++i) {
    if ( gdjs.intro1Code.GDfondObjects1[i].getAnimation() == 1 ) {
        gdjs.intro1Code.condition1IsTrue_0.val = true;
        gdjs.intro1Code.GDfondObjects1[k] = gdjs.intro1Code.GDfondObjects1[i];
        ++k;
    }
}
gdjs.intro1Code.GDfondObjects1.length = k;}}
if (gdjs.intro1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.intro1Code.GDfondObjects1 */
{for(var i = 0, len = gdjs.intro1Code.GDfondObjects1.length ;i < len;++i) {
    gdjs.intro1Code.GDfondObjects1[i].setAnimation(2);
}
}}

}


{

gdjs.intro1Code.GDfondObjects1.createFrom(runtimeScene.getObjects("fond"));

gdjs.intro1Code.condition0IsTrue_0.val = false;
gdjs.intro1Code.condition1IsTrue_0.val = false;
{
gdjs.intro1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.intro1Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.intro1Code.GDfondObjects1.length;i<l;++i) {
    if ( gdjs.intro1Code.GDfondObjects1[i].getAnimation() == 0 ) {
        gdjs.intro1Code.condition1IsTrue_0.val = true;
        gdjs.intro1Code.GDfondObjects1[k] = gdjs.intro1Code.GDfondObjects1[i];
        ++k;
    }
}
gdjs.intro1Code.GDfondObjects1.length = k;}}
if (gdjs.intro1Code.condition1IsTrue_0.val) {
/* Reuse gdjs.intro1Code.GDfondObjects1 */
{for(var i = 0, len = gdjs.intro1Code.GDfondObjects1.length ;i < len;++i) {
    gdjs.intro1Code.GDfondObjects1[i].setAnimation(1);
}
}}

}


}; //End of gdjs.intro1Code.eventsList0x5b7028


gdjs.intro1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.intro1Code.GDfondObjects1.length = 0;
gdjs.intro1Code.GDfondObjects2.length = 0;

gdjs.intro1Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['intro1Code'] = gdjs.intro1Code;
