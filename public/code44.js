gdjs.MON3Code = {};
gdjs.MON3Code.GDNewObjectObjects1= [];
gdjs.MON3Code.GDNewObjectObjects2= [];

gdjs.MON3Code.conditionTrue_0 = {val:false};
gdjs.MON3Code.condition0IsTrue_0 = {val:false};
gdjs.MON3Code.condition1IsTrue_0 = {val:false};


gdjs.MON3Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.MON3Code.condition0IsTrue_0.val = false;
{
gdjs.MON3Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MON3Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


}; //End of gdjs.MON3Code.eventsList0x5b7028


gdjs.MON3Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MON3Code.GDNewObjectObjects1.length = 0;
gdjs.MON3Code.GDNewObjectObjects2.length = 0;

gdjs.MON3Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['MON3Code'] = gdjs.MON3Code;
