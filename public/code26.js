gdjs.RAM05Code = {};
gdjs.RAM05Code.GDNewObjectObjects1= [];
gdjs.RAM05Code.GDNewObjectObjects2= [];

gdjs.RAM05Code.conditionTrue_0 = {val:false};
gdjs.RAM05Code.condition0IsTrue_0 = {val:false};
gdjs.RAM05Code.condition1IsTrue_0 = {val:false};


gdjs.RAM05Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.RAM05Code.condition0IsTrue_0.val = false;
{
gdjs.RAM05Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.RAM05Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(1).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


}; //End of gdjs.RAM05Code.eventsList0x5b7028


gdjs.RAM05Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.RAM05Code.GDNewObjectObjects1.length = 0;
gdjs.RAM05Code.GDNewObjectObjects2.length = 0;

gdjs.RAM05Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['RAM05Code'] = gdjs.RAM05Code;
