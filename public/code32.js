gdjs.BUGCode = {};
gdjs.BUGCode.GDNewObjectObjects1= [];
gdjs.BUGCode.GDNewObjectObjects2= [];

gdjs.BUGCode.conditionTrue_0 = {val:false};
gdjs.BUGCode.condition0IsTrue_0 = {val:false};
gdjs.BUGCode.condition1IsTrue_0 = {val:false};


gdjs.BUGCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.BUGCode.condition0IsTrue_0.val = false;
{
gdjs.BUGCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.BUGCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T1");
}}

}


{


gdjs.BUGCode.condition0IsTrue_0.val = false;
{
gdjs.BUGCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T1");
}if (gdjs.BUGCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER04", false);
}}

}


}; //End of gdjs.BUGCode.eventsList0x5b7028


gdjs.BUGCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.BUGCode.GDNewObjectObjects1.length = 0;
gdjs.BUGCode.GDNewObjectObjects2.length = 0;

gdjs.BUGCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['BUGCode'] = gdjs.BUGCode;
