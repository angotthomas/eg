gdjs.SORTIEPCode = {};
gdjs.SORTIEPCode.GDNewObjectObjects1= [];
gdjs.SORTIEPCode.GDNewObjectObjects2= [];

gdjs.SORTIEPCode.conditionTrue_0 = {val:false};
gdjs.SORTIEPCode.condition0IsTrue_0 = {val:false};
gdjs.SORTIEPCode.condition1IsTrue_0 = {val:false};


gdjs.SORTIEPCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SORTIEPCode.condition0IsTrue_0.val = false;
{
gdjs.SORTIEPCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.SORTIEPCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


{


gdjs.SORTIEPCode.condition0IsTrue_0.val = false;
{
gdjs.SORTIEPCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SORTIEPCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "reveil.wav", false, 100, 1);
}}

}


}; //End of gdjs.SORTIEPCode.eventsList0x5b7028


gdjs.SORTIEPCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SORTIEPCode.GDNewObjectObjects1.length = 0;
gdjs.SORTIEPCode.GDNewObjectObjects2.length = 0;

gdjs.SORTIEPCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SORTIEPCode'] = gdjs.SORTIEPCode;
