gdjs.SORTIE4Code = {};
gdjs.SORTIE4Code.GDNewObjectObjects1= [];
gdjs.SORTIE4Code.GDNewObjectObjects2= [];
gdjs.SORTIE4Code.GDCHRONOObjects1= [];
gdjs.SORTIE4Code.GDCHRONOObjects2= [];

gdjs.SORTIE4Code.conditionTrue_0 = {val:false};
gdjs.SORTIE4Code.condition0IsTrue_0 = {val:false};
gdjs.SORTIE4Code.condition1IsTrue_0 = {val:false};
gdjs.SORTIE4Code.condition2IsTrue_0 = {val:false};
gdjs.SORTIE4Code.conditionTrue_1 = {val:false};
gdjs.SORTIE4Code.condition0IsTrue_1 = {val:false};
gdjs.SORTIE4Code.condition1IsTrue_1 = {val:false};
gdjs.SORTIE4Code.condition2IsTrue_1 = {val:false};


gdjs.SORTIE4Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
{gdjs.SORTIE4Code.conditionTrue_1 = gdjs.SORTIE4Code.condition0IsTrue_0;
gdjs.SORTIE4Code.condition0IsTrue_1.val = false;
gdjs.SORTIE4Code.condition1IsTrue_1.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "R");
if( gdjs.SORTIE4Code.condition0IsTrue_1.val ) {
    gdjs.SORTIE4Code.conditionTrue_1.val = true;
}
}
{
gdjs.SORTIE4Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "r");
if( gdjs.SORTIE4Code.condition1IsTrue_1.val ) {
    gdjs.SORTIE4Code.conditionTrue_1.val = true;
}
}
{
}
}
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIE5", false);
}{gdjs.evtTools.sound.playMusic(runtimeScene, "bipoui.wav", false, 100, 1);
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T2");
}{gdjs.evtTools.sound.playSound(runtimeScene, "chrono.wav", true, 100, 1);
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 0, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("9");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 1, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("8");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 2, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("7");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("6");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 4, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("5");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 5, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("4");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 6, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("3");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 7, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("2");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 8, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("1");
}
}}

}


{


gdjs.SORTIE4Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE4Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 9, "T2");
}if (gdjs.SORTIE4Code.condition0IsTrue_0.val) {
gdjs.SORTIE4Code.GDCHRONOObjects1.createFrom(runtimeScene.getObjects("CHRONO"));
{for(var i = 0, len = gdjs.SORTIE4Code.GDCHRONOObjects1.length ;i < len;++i) {
    gdjs.SORTIE4Code.GDCHRONOObjects1[i].setString("0");
}
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIEP", false);
}}

}


{


{
}

}


}; //End of gdjs.SORTIE4Code.eventsList0x5b7028


gdjs.SORTIE4Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SORTIE4Code.GDNewObjectObjects1.length = 0;
gdjs.SORTIE4Code.GDNewObjectObjects2.length = 0;
gdjs.SORTIE4Code.GDCHRONOObjects1.length = 0;
gdjs.SORTIE4Code.GDCHRONOObjects2.length = 0;

gdjs.SORTIE4Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SORTIE4Code'] = gdjs.SORTIE4Code;
