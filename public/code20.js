gdjs.PROC1Code = {};
gdjs.PROC1Code.GDNewObjectObjects1= [];
gdjs.PROC1Code.GDNewObjectObjects2= [];

gdjs.PROC1Code.conditionTrue_0 = {val:false};
gdjs.PROC1Code.condition0IsTrue_0 = {val:false};
gdjs.PROC1Code.condition1IsTrue_0 = {val:false};


gdjs.PROC1Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.PROC1Code.condition0IsTrue_0.val = false;
{
gdjs.PROC1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.PROC1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "PROC2", true);
}}

}


}; //End of gdjs.PROC1Code.eventsList0x5b7028


gdjs.PROC1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.PROC1Code.GDNewObjectObjects1.length = 0;
gdjs.PROC1Code.GDNewObjectObjects2.length = 0;

gdjs.PROC1Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['PROC1Code'] = gdjs.PROC1Code;
