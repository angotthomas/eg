gdjs.CM03Code = {};
gdjs.CM03Code.GDNewObjectObjects1= [];
gdjs.CM03Code.GDNewObjectObjects2= [];
gdjs.CM03Code.GDNewObject2Objects1= [];
gdjs.CM03Code.GDNewObject2Objects2= [];

gdjs.CM03Code.conditionTrue_0 = {val:false};
gdjs.CM03Code.condition0IsTrue_0 = {val:false};
gdjs.CM03Code.condition1IsTrue_0 = {val:false};


gdjs.CM03Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.CM03Code.condition0IsTrue_0.val = false;
{
gdjs.CM03Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.CM03Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(5).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


}; //End of gdjs.CM03Code.eventsList0x5b7028


gdjs.CM03Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.CM03Code.GDNewObjectObjects1.length = 0;
gdjs.CM03Code.GDNewObjectObjects2.length = 0;
gdjs.CM03Code.GDNewObject2Objects1.length = 0;
gdjs.CM03Code.GDNewObject2Objects2.length = 0;

gdjs.CM03Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['CM03Code'] = gdjs.CM03Code;
