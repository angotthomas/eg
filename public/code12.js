gdjs.FICHIER12Code = {};
gdjs.FICHIER12Code.GDNewObjectObjects1= [];
gdjs.FICHIER12Code.GDNewObjectObjects2= [];
gdjs.FICHIER12Code.GDCObjects1= [];
gdjs.FICHIER12Code.GDCObjects2= [];
gdjs.FICHIER12Code.GDRETOURObjects1= [];
gdjs.FICHIER12Code.GDRETOURObjects2= [];
gdjs.FICHIER12Code.GDBObjects1= [];
gdjs.FICHIER12Code.GDBObjects2= [];

gdjs.FICHIER12Code.conditionTrue_0 = {val:false};
gdjs.FICHIER12Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER12Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER12Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER12Code.GDRETOURObjects1});gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDBObjects1Objects = Hashtable.newFrom({"B": gdjs.FICHIER12Code.GDBObjects1});gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDCObjects1Objects = Hashtable.newFrom({"C": gdjs.FICHIER12Code.GDCObjects1});gdjs.FICHIER12Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER12Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER12Code.condition0IsTrue_0.val = false;
gdjs.FICHIER12Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER12Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER12Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER12Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER12Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER07", true);
}}

}


{

gdjs.FICHIER12Code.GDBObjects1.createFrom(runtimeScene.getObjects("B"));

gdjs.FICHIER12Code.condition0IsTrue_0.val = false;
gdjs.FICHIER12Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER12Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER12Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER12Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDBObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER12Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "B.ogg", false, 100, 1);
}}

}


{

gdjs.FICHIER12Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));

gdjs.FICHIER12Code.condition0IsTrue_0.val = false;
gdjs.FICHIER12Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER12Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER12Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER12Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER12Code.mapOfGDgdjs_46FICHIER12Code_46GDCObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER12Code.condition1IsTrue_0.val) {
{gdjs.evtTools.sound.playSound(runtimeScene, "C.ogg", false, 100, 1);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER12Code.eventsList0x5b7028


gdjs.FICHIER12Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER12Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER12Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER12Code.GDCObjects1.length = 0;
gdjs.FICHIER12Code.GDCObjects2.length = 0;
gdjs.FICHIER12Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER12Code.GDRETOURObjects2.length = 0;
gdjs.FICHIER12Code.GDBObjects1.length = 0;
gdjs.FICHIER12Code.GDBObjects2.length = 0;

gdjs.FICHIER12Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER12Code'] = gdjs.FICHIER12Code;
