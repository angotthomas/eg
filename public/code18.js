gdjs.FICHIERFINCode = {};
gdjs.FICHIERFINCode.GDNewObjectObjects1= [];
gdjs.FICHIERFINCode.GDNewObjectObjects2= [];

gdjs.FICHIERFINCode.conditionTrue_0 = {val:false};
gdjs.FICHIERFINCode.condition0IsTrue_0 = {val:false};
gdjs.FICHIERFINCode.condition1IsTrue_0 = {val:false};
gdjs.FICHIERFINCode.condition2IsTrue_0 = {val:false};


gdjs.FICHIERFINCode.mapOfGDgdjs_46FICHIERFINCode_46GDNewObjectObjects1Objects = Hashtable.newFrom({"NewObject": gdjs.FICHIERFINCode.GDNewObjectObjects1});gdjs.FICHIERFINCode.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIERFINCode.GDNewObjectObjects1.createFrom(runtimeScene.getObjects("NewObject"));

gdjs.FICHIERFINCode.condition0IsTrue_0.val = false;
gdjs.FICHIERFINCode.condition1IsTrue_0.val = false;
{
gdjs.FICHIERFINCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIERFINCode.condition0IsTrue_0.val ) {
{
gdjs.FICHIERFINCode.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIERFINCode.mapOfGDgdjs_46FICHIERFINCode_46GDNewObjectObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIERFINCode.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}{runtimeScene.getGame().getVariables().getFromIndex(2).setNumber(1);
}}

}


{


{
}

}


}; //End of gdjs.FICHIERFINCode.eventsList0x5b7028


gdjs.FICHIERFINCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIERFINCode.GDNewObjectObjects1.length = 0;
gdjs.FICHIERFINCode.GDNewObjectObjects2.length = 0;

gdjs.FICHIERFINCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIERFINCode'] = gdjs.FICHIERFINCode;
