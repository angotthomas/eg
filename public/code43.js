gdjs.MON2Code = {};
gdjs.MON2Code.GDNewObjectObjects1= [];
gdjs.MON2Code.GDNewObjectObjects2= [];
gdjs.MON2Code.GDTEXTECODEObjects1= [];
gdjs.MON2Code.GDTEXTECODEObjects2= [];
gdjs.MON2Code.GDSAISIEObjects1= [];
gdjs.MON2Code.GDSAISIEObjects2= [];
gdjs.MON2Code.GDNewObject2Objects1= [];
gdjs.MON2Code.GDNewObject2Objects2= [];

gdjs.MON2Code.conditionTrue_0 = {val:false};
gdjs.MON2Code.condition0IsTrue_0 = {val:false};
gdjs.MON2Code.condition1IsTrue_0 = {val:false};
gdjs.MON2Code.condition2IsTrue_0 = {val:false};
gdjs.MON2Code.conditionTrue_1 = {val:false};
gdjs.MON2Code.condition0IsTrue_1 = {val:false};
gdjs.MON2Code.condition1IsTrue_1 = {val:false};
gdjs.MON2Code.condition2IsTrue_1 = {val:false};


gdjs.MON2Code.mapOfGDgdjs_46MON2Code_46GDNewObject2Objects1Objects = Hashtable.newFrom({"NewObject2": gdjs.MON2Code.GDNewObject2Objects1});gdjs.MON2Code.eventsList0x5b7028 = function(runtimeScene) {

{


{
gdjs.MON2Code.GDSAISIEObjects1.createFrom(runtimeScene.getObjects("SAISIE"));
gdjs.MON2Code.GDTEXTECODEObjects1.createFrom(runtimeScene.getObjects("TEXTECODE"));
{for(var i = 0, len = gdjs.MON2Code.GDTEXTECODEObjects1.length ;i < len;++i) {
    gdjs.MON2Code.GDTEXTECODEObjects1[i].setString((( gdjs.MON2Code.GDSAISIEObjects1.length === 0 ) ? "" :gdjs.MON2Code.GDSAISIEObjects1[0].getString()));
}
}{runtimeScene.getVariables().getFromIndex(0).setNumber(gdjs.evtTools.string.strLen((( gdjs.MON2Code.GDTEXTECODEObjects1.length === 0 ) ? "" :gdjs.MON2Code.GDTEXTECODEObjects1[0].getString())));
}}

}


{

gdjs.MON2Code.GDTEXTECODEObjects1.createFrom(runtimeScene.getObjects("TEXTECODE"));

gdjs.MON2Code.condition0IsTrue_0.val = false;
gdjs.MON2Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.MON2Code.GDTEXTECODEObjects1.length;i<l;++i) {
    if ( gdjs.MON2Code.GDTEXTECODEObjects1[i].getString() == "255000000" ) {
        gdjs.MON2Code.condition0IsTrue_0.val = true;
        gdjs.MON2Code.GDTEXTECODEObjects1[k] = gdjs.MON2Code.GDTEXTECODEObjects1[i];
        ++k;
    }
}
gdjs.MON2Code.GDTEXTECODEObjects1.length = k;}if ( gdjs.MON2Code.condition0IsTrue_0.val ) {
{
{gdjs.MON2Code.conditionTrue_1 = gdjs.MON2Code.condition1IsTrue_0;
gdjs.MON2Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(10027956);
}
}}
if (gdjs.MON2Code.condition1IsTrue_0.val) {
gdjs.MON2Code.GDSAISIEObjects1.createFrom(runtimeScene.getObjects("SAISIE"));
/* Reuse gdjs.MON2Code.GDTEXTECODEObjects1 */
{gdjs.evtTools.sound.playMusic(runtimeScene, "SF-yehaw.mp3", false, 100, 1);
}{for(var i = 0, len = gdjs.MON2Code.GDSAISIEObjects1.length ;i < len;++i) {
    gdjs.MON2Code.GDSAISIEObjects1[i].activate(false);
}
}{for(var i = 0, len = gdjs.MON2Code.GDTEXTECODEObjects1.length ;i < len;++i) {
    gdjs.MON2Code.GDTEXTECODEObjects1[i].setString("255000000");
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T1");
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "T1");
}}

}


{

gdjs.MON2Code.GDTEXTECODEObjects1.createFrom(runtimeScene.getObjects("TEXTECODE"));

gdjs.MON2Code.condition0IsTrue_0.val = false;
gdjs.MON2Code.condition1IsTrue_0.val = false;
{
gdjs.MON2Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 9;
}if ( gdjs.MON2Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.MON2Code.GDTEXTECODEObjects1.length;i<l;++i) {
    if ( gdjs.MON2Code.GDTEXTECODEObjects1[i].getString() != "255000000" ) {
        gdjs.MON2Code.condition1IsTrue_0.val = true;
        gdjs.MON2Code.GDTEXTECODEObjects1[k] = gdjs.MON2Code.GDTEXTECODEObjects1[i];
        ++k;
    }
}
gdjs.MON2Code.GDTEXTECODEObjects1.length = k;}}
if (gdjs.MON2Code.condition1IsTrue_0.val) {
gdjs.MON2Code.GDSAISIEObjects1.createFrom(runtimeScene.getObjects("SAISIE"));
{for(var i = 0, len = gdjs.MON2Code.GDSAISIEObjects1.length ;i < len;++i) {
    gdjs.MON2Code.GDSAISIEObjects1[i].setString("");
}
}}

}


{


gdjs.MON2Code.condition0IsTrue_0.val = false;
{
gdjs.MON2Code.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().getFromIndex(0)) == 0;
}if (gdjs.MON2Code.condition0IsTrue_0.val) {
gdjs.MON2Code.GDTEXTECODEObjects1.createFrom(runtimeScene.getObjects("TEXTECODE"));
{for(var i = 0, len = gdjs.MON2Code.GDTEXTECODEObjects1.length ;i < len;++i) {
    gdjs.MON2Code.GDTEXTECODEObjects1[i].setString("XXXXXXXXX");
}
}}

}


{


gdjs.MON2Code.condition0IsTrue_0.val = false;
{
gdjs.MON2Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.MON2Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "T1");
}}

}


{


gdjs.MON2Code.condition0IsTrue_0.val = false;
{
gdjs.MON2Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T1");
}if (gdjs.MON2Code.condition0IsTrue_0.val) {
{runtimeScene.getGame().getVariables().getFromIndex(3).setNumber(1);
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MON3", true);
}}

}


{

gdjs.MON2Code.GDNewObject2Objects1.createFrom(runtimeScene.getObjects("NewObject2"));

gdjs.MON2Code.condition0IsTrue_0.val = false;
gdjs.MON2Code.condition1IsTrue_0.val = false;
{
gdjs.MON2Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.MON2Code.mapOfGDgdjs_46MON2Code_46GDNewObject2Objects1Objects, runtimeScene, true, false);
}if ( gdjs.MON2Code.condition0IsTrue_0.val ) {
{
gdjs.MON2Code.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.MON2Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


}; //End of gdjs.MON2Code.eventsList0x5b7028


gdjs.MON2Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MON2Code.GDNewObjectObjects1.length = 0;
gdjs.MON2Code.GDNewObjectObjects2.length = 0;
gdjs.MON2Code.GDTEXTECODEObjects1.length = 0;
gdjs.MON2Code.GDTEXTECODEObjects2.length = 0;
gdjs.MON2Code.GDSAISIEObjects1.length = 0;
gdjs.MON2Code.GDSAISIEObjects2.length = 0;
gdjs.MON2Code.GDNewObject2Objects1.length = 0;
gdjs.MON2Code.GDNewObject2Objects2.length = 0;

gdjs.MON2Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['MON2Code'] = gdjs.MON2Code;
