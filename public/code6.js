gdjs.FICHIER06Code = {};
gdjs.FICHIER06Code.GDNewObjectObjects1= [];
gdjs.FICHIER06Code.GDNewObjectObjects2= [];
gdjs.FICHIER06Code.GDTESTObjects1= [];
gdjs.FICHIER06Code.GDTESTObjects2= [];
gdjs.FICHIER06Code.GDREPONSEObjects1= [];
gdjs.FICHIER06Code.GDREPONSEObjects2= [];
gdjs.FICHIER06Code.GDVALIDERObjects1= [];
gdjs.FICHIER06Code.GDVALIDERObjects2= [];
gdjs.FICHIER06Code.GDSAISIEObjects1= [];
gdjs.FICHIER06Code.GDSAISIEObjects2= [];

gdjs.FICHIER06Code.conditionTrue_0 = {val:false};
gdjs.FICHIER06Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER06Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER06Code.condition2IsTrue_0 = {val:false};
gdjs.FICHIER06Code.conditionTrue_1 = {val:false};
gdjs.FICHIER06Code.condition0IsTrue_1 = {val:false};
gdjs.FICHIER06Code.condition1IsTrue_1 = {val:false};
gdjs.FICHIER06Code.condition2IsTrue_1 = {val:false};


gdjs.FICHIER06Code.mapOfGDgdjs_46FICHIER06Code_46GDNewObjectObjects1Objects = Hashtable.newFrom({"NewObject": gdjs.FICHIER06Code.GDNewObjectObjects1});gdjs.FICHIER06Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.FICHIER06Code.condition0IsTrue_0.val = false;
{
gdjs.FICHIER06Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.FICHIER06Code.condition0IsTrue_0.val) {
gdjs.FICHIER06Code.GDREPONSEObjects1.createFrom(runtimeScene.getObjects("REPONSE"));
{for(var i = 0, len = gdjs.FICHIER06Code.GDREPONSEObjects1.length ;i < len;++i) {
    gdjs.FICHIER06Code.GDREPONSEObjects1[i].setString("");
}
}{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "T1");
}}

}


{

gdjs.FICHIER06Code.GDNewObjectObjects1.createFrom(runtimeScene.getObjects("NewObject"));

gdjs.FICHIER06Code.condition0IsTrue_0.val = false;
gdjs.FICHIER06Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER06Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER06Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER06Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER06Code.mapOfGDgdjs_46FICHIER06Code_46GDNewObjectObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER06Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER04", false);
}}

}


{

gdjs.FICHIER06Code.GDREPONSEObjects1.createFrom(runtimeScene.getObjects("REPONSE"));

gdjs.FICHIER06Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.FICHIER06Code.GDREPONSEObjects1.length;i<l;++i) {
    if ( gdjs.FICHIER06Code.GDREPONSEObjects1[i].getString() != "RBORESCENCE" ) {
        gdjs.FICHIER06Code.condition0IsTrue_0.val = true;
        gdjs.FICHIER06Code.GDREPONSEObjects1[k] = gdjs.FICHIER06Code.GDREPONSEObjects1[i];
        ++k;
    }
}
gdjs.FICHIER06Code.GDREPONSEObjects1.length = k;}if (gdjs.FICHIER06Code.condition0IsTrue_0.val) {
/* Reuse gdjs.FICHIER06Code.GDREPONSEObjects1 */
gdjs.FICHIER06Code.GDSAISIEObjects1.createFrom(runtimeScene.getObjects("SAISIE"));
{for(var i = 0, len = gdjs.FICHIER06Code.GDREPONSEObjects1.length ;i < len;++i) {
    gdjs.FICHIER06Code.GDREPONSEObjects1[i].setString(gdjs.evtTools.string.toUpperCase((( gdjs.FICHIER06Code.GDSAISIEObjects1.length === 0 ) ? "" :gdjs.FICHIER06Code.GDSAISIEObjects1[0].getString())));
}
}}

}


{

gdjs.FICHIER06Code.GDREPONSEObjects1.createFrom(runtimeScene.getObjects("REPONSE"));

gdjs.FICHIER06Code.condition0IsTrue_0.val = false;
gdjs.FICHIER06Code.condition1IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.FICHIER06Code.GDREPONSEObjects1.length;i<l;++i) {
    if ( gdjs.FICHIER06Code.GDREPONSEObjects1[i].getString() == "RBORESCENCE" ) {
        gdjs.FICHIER06Code.condition0IsTrue_0.val = true;
        gdjs.FICHIER06Code.GDREPONSEObjects1[k] = gdjs.FICHIER06Code.GDREPONSEObjects1[i];
        ++k;
    }
}
gdjs.FICHIER06Code.GDREPONSEObjects1.length = k;}if ( gdjs.FICHIER06Code.condition0IsTrue_0.val ) {
{
{gdjs.FICHIER06Code.conditionTrue_1 = gdjs.FICHIER06Code.condition1IsTrue_0;
gdjs.FICHIER06Code.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(9797588);
}
}}
if (gdjs.FICHIER06Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "T1");
}{gdjs.evtTools.sound.playSound(runtimeScene, "SF-yehaw.mp3", false, 100, 1);
}}

}


{


{
}

}


{


gdjs.FICHIER06Code.condition0IsTrue_0.val = false;
{
gdjs.FICHIER06Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T1");
}if (gdjs.FICHIER06Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "T1");
}{gdjs.evtTools.runtimeScene.removeTimer(runtimeScene, "T1");
}{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIERFIN", false);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER06Code.eventsList0x5b7028


gdjs.FICHIER06Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER06Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER06Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER06Code.GDTESTObjects1.length = 0;
gdjs.FICHIER06Code.GDTESTObjects2.length = 0;
gdjs.FICHIER06Code.GDREPONSEObjects1.length = 0;
gdjs.FICHIER06Code.GDREPONSEObjects2.length = 0;
gdjs.FICHIER06Code.GDVALIDERObjects1.length = 0;
gdjs.FICHIER06Code.GDVALIDERObjects2.length = 0;
gdjs.FICHIER06Code.GDSAISIEObjects1.length = 0;
gdjs.FICHIER06Code.GDSAISIEObjects2.length = 0;

gdjs.FICHIER06Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER06Code'] = gdjs.FICHIER06Code;
