gdjs.CM01Code = {};
gdjs.CM01Code.GDNewObjectObjects1= [];
gdjs.CM01Code.GDNewObjectObjects2= [];

gdjs.CM01Code.conditionTrue_0 = {val:false};
gdjs.CM01Code.condition0IsTrue_0 = {val:false};
gdjs.CM01Code.condition1IsTrue_0 = {val:false};


gdjs.CM01Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.CM01Code.condition0IsTrue_0.val = false;
{
gdjs.CM01Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.CM01Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "CM02", false);
}}

}


}; //End of gdjs.CM01Code.eventsList0x5b7028


gdjs.CM01Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.CM01Code.GDNewObjectObjects1.length = 0;
gdjs.CM01Code.GDNewObjectObjects2.length = 0;

gdjs.CM01Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['CM01Code'] = gdjs.CM01Code;
