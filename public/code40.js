gdjs.SORTIEGCode = {};
gdjs.SORTIEGCode.GDNewObjectObjects1= [];
gdjs.SORTIEGCode.GDNewObjectObjects2= [];

gdjs.SORTIEGCode.conditionTrue_0 = {val:false};
gdjs.SORTIEGCode.condition0IsTrue_0 = {val:false};
gdjs.SORTIEGCode.condition1IsTrue_0 = {val:false};


gdjs.SORTIEGCode.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SORTIEGCode.condition0IsTrue_0.val = false;
{
gdjs.SORTIEGCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.SORTIEGCode.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", false);
}}

}


{


gdjs.SORTIEGCode.condition0IsTrue_0.val = false;
{
gdjs.SORTIEGCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.SORTIEGCode.condition0IsTrue_0.val) {
{gdjs.evtTools.sound.playMusic(runtimeScene, "APPLAU.wav", false, 100, 1);
}}

}


}; //End of gdjs.SORTIEGCode.eventsList0x5b7028


gdjs.SORTIEGCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SORTIEGCode.GDNewObjectObjects1.length = 0;
gdjs.SORTIEGCode.GDNewObjectObjects2.length = 0;

gdjs.SORTIEGCode.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SORTIEGCode'] = gdjs.SORTIEGCode;
