gdjs.RAM03Code = {};
gdjs.RAM03Code.GDNewObjectObjects1= [];
gdjs.RAM03Code.GDNewObjectObjects2= [];
gdjs.RAM03Code.GDNewObjectObjects3= [];
gdjs.RAM03Code.GDNewObject2Objects1= [];
gdjs.RAM03Code.GDNewObject2Objects2= [];
gdjs.RAM03Code.GDNewObject2Objects3= [];
gdjs.RAM03Code.GDBIN1Objects1= [];
gdjs.RAM03Code.GDBIN1Objects2= [];
gdjs.RAM03Code.GDBIN1Objects3= [];
gdjs.RAM03Code.GDTEXTE2Objects1= [];
gdjs.RAM03Code.GDTEXTE2Objects2= [];
gdjs.RAM03Code.GDTEXTE2Objects3= [];
gdjs.RAM03Code.GDTEXTEObjects1= [];
gdjs.RAM03Code.GDTEXTEObjects2= [];
gdjs.RAM03Code.GDTEXTEObjects3= [];

gdjs.RAM03Code.conditionTrue_0 = {val:false};
gdjs.RAM03Code.condition0IsTrue_0 = {val:false};
gdjs.RAM03Code.condition1IsTrue_0 = {val:false};
gdjs.RAM03Code.condition2IsTrue_0 = {val:false};


gdjs.RAM03Code.mapOfGDgdjs_46RAM03Code_46GDBIN1Objects2Objects = Hashtable.newFrom({"BIN1": gdjs.RAM03Code.GDBIN1Objects2});gdjs.RAM03Code.eventsList0x95218c = function(runtimeScene) {

{

/* Reuse gdjs.RAM03Code.GDBIN1Objects2 */

gdjs.RAM03Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.RAM03Code.GDBIN1Objects2.length;i<l;++i) {
    if ( gdjs.RAM03Code.GDBIN1Objects2[i].getAnimation() > 2 ) {
        gdjs.RAM03Code.condition0IsTrue_0.val = true;
        gdjs.RAM03Code.GDBIN1Objects2[k] = gdjs.RAM03Code.GDBIN1Objects2[i];
        ++k;
    }
}
gdjs.RAM03Code.GDBIN1Objects2.length = k;}if (gdjs.RAM03Code.condition0IsTrue_0.val) {
/* Reuse gdjs.RAM03Code.GDBIN1Objects2 */
{for(var i = 0, len = gdjs.RAM03Code.GDBIN1Objects2.length ;i < len;++i) {
    gdjs.RAM03Code.GDBIN1Objects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.RAM03Code.eventsList0x95218c
gdjs.RAM03Code.mapOfGDgdjs_46RAM03Code_46GDTEXTE2Objects2Objects = Hashtable.newFrom({"TEXTE2": gdjs.RAM03Code.GDTEXTE2Objects2});gdjs.RAM03Code.eventsList0x964e04 = function(runtimeScene) {

{

gdjs.RAM03Code.GDBIN1Objects2.createFrom(runtimeScene.getObjects("BIN1"));

gdjs.RAM03Code.condition0IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.RAM03Code.GDBIN1Objects2.length;i<l;++i) {
    if ( gdjs.RAM03Code.GDBIN1Objects2[i].getAnimation() > 2 ) {
        gdjs.RAM03Code.condition0IsTrue_0.val = true;
        gdjs.RAM03Code.GDBIN1Objects2[k] = gdjs.RAM03Code.GDBIN1Objects2[i];
        ++k;
    }
}
gdjs.RAM03Code.GDBIN1Objects2.length = k;}if (gdjs.RAM03Code.condition0IsTrue_0.val) {
/* Reuse gdjs.RAM03Code.GDBIN1Objects2 */
{for(var i = 0, len = gdjs.RAM03Code.GDBIN1Objects2.length ;i < len;++i) {
    gdjs.RAM03Code.GDBIN1Objects2[i].setAnimation(0);
}
}}

}


}; //End of gdjs.RAM03Code.eventsList0x964e04
gdjs.RAM03Code.eventsList0x96499c = function(runtimeScene) {

{

gdjs.RAM03Code.GDBIN1Objects2.createFrom(runtimeScene.getObjects("BIN1"));

gdjs.RAM03Code.condition0IsTrue_0.val = false;
gdjs.RAM03Code.condition1IsTrue_0.val = false;
{
gdjs.RAM03Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.RAM03Code.condition0IsTrue_0.val ) {
{
gdjs.RAM03Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RAM03Code.mapOfGDgdjs_46RAM03Code_46GDBIN1Objects2Objects, runtimeScene, true, false);
}}
if (gdjs.RAM03Code.condition1IsTrue_0.val) {
/* Reuse gdjs.RAM03Code.GDBIN1Objects2 */
{for(var i = 0, len = gdjs.RAM03Code.GDBIN1Objects2.length ;i < len;++i) {
    gdjs.RAM03Code.GDBIN1Objects2[i].setAnimation((gdjs.RAM03Code.GDBIN1Objects2[i].getAnimation()) + 1);
}
}
{ //Subevents
gdjs.RAM03Code.eventsList0x95218c(runtimeScene);} //End of subevents
}

}


{

gdjs.RAM03Code.GDTEXTE2Objects2.createFrom(runtimeScene.getObjects("TEXTE2"));

gdjs.RAM03Code.condition0IsTrue_0.val = false;
gdjs.RAM03Code.condition1IsTrue_0.val = false;
{
gdjs.RAM03Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.RAM03Code.condition0IsTrue_0.val ) {
{
gdjs.RAM03Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.RAM03Code.mapOfGDgdjs_46RAM03Code_46GDTEXTE2Objects2Objects, runtimeScene, true, false);
}}
if (gdjs.RAM03Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "RAM04", false);
}
{ //Subevents
gdjs.RAM03Code.eventsList0x964e04(runtimeScene);} //End of subevents
}

}


{


{
}

}


}; //End of gdjs.RAM03Code.eventsList0x96499c
gdjs.RAM03Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.RAM03Code.eventsList0x96499c(runtimeScene);
}


}; //End of gdjs.RAM03Code.eventsList0x5b7028


gdjs.RAM03Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.RAM03Code.GDNewObjectObjects1.length = 0;
gdjs.RAM03Code.GDNewObjectObjects2.length = 0;
gdjs.RAM03Code.GDNewObjectObjects3.length = 0;
gdjs.RAM03Code.GDNewObject2Objects1.length = 0;
gdjs.RAM03Code.GDNewObject2Objects2.length = 0;
gdjs.RAM03Code.GDNewObject2Objects3.length = 0;
gdjs.RAM03Code.GDBIN1Objects1.length = 0;
gdjs.RAM03Code.GDBIN1Objects2.length = 0;
gdjs.RAM03Code.GDBIN1Objects3.length = 0;
gdjs.RAM03Code.GDTEXTE2Objects1.length = 0;
gdjs.RAM03Code.GDTEXTE2Objects2.length = 0;
gdjs.RAM03Code.GDTEXTE2Objects3.length = 0;
gdjs.RAM03Code.GDTEXTEObjects1.length = 0;
gdjs.RAM03Code.GDTEXTEObjects2.length = 0;
gdjs.RAM03Code.GDTEXTEObjects3.length = 0;

gdjs.RAM03Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['RAM03Code'] = gdjs.RAM03Code;
