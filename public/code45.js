gdjs.MON1Code = {};
gdjs.MON1Code.GDNewObjectObjects1= [];
gdjs.MON1Code.GDNewObjectObjects2= [];

gdjs.MON1Code.conditionTrue_0 = {val:false};
gdjs.MON1Code.condition0IsTrue_0 = {val:false};
gdjs.MON1Code.condition1IsTrue_0 = {val:false};


gdjs.MON1Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.MON1Code.condition0IsTrue_0.val = false;
{
gdjs.MON1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.MON1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MON2", true);
}}

}


}; //End of gdjs.MON1Code.eventsList0x5b7028


gdjs.MON1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.MON1Code.GDNewObjectObjects1.length = 0;
gdjs.MON1Code.GDNewObjectObjects2.length = 0;

gdjs.MON1Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['MON1Code'] = gdjs.MON1Code;
