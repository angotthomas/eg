gdjs.FICHIER07Code = {};
gdjs.FICHIER07Code.GDNewObjectObjects1= [];
gdjs.FICHIER07Code.GDNewObjectObjects2= [];
gdjs.FICHIER07Code.GDRETOURObjects1= [];
gdjs.FICHIER07Code.GDRETOURObjects2= [];
gdjs.FICHIER07Code.GDMONDEObjects1= [];
gdjs.FICHIER07Code.GDMONDEObjects2= [];
gdjs.FICHIER07Code.GDMUSIQUESObjects1= [];
gdjs.FICHIER07Code.GDMUSIQUESObjects2= [];
gdjs.FICHIER07Code.GDIMAGESObjects1= [];
gdjs.FICHIER07Code.GDIMAGESObjects2= [];

gdjs.FICHIER07Code.conditionTrue_0 = {val:false};
gdjs.FICHIER07Code.condition0IsTrue_0 = {val:false};
gdjs.FICHIER07Code.condition1IsTrue_0 = {val:false};
gdjs.FICHIER07Code.condition2IsTrue_0 = {val:false};


gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.FICHIER07Code.GDRETOURObjects1});gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDIMAGESObjects1Objects = Hashtable.newFrom({"IMAGES": gdjs.FICHIER07Code.GDIMAGESObjects1});gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDMUSIQUESObjects1Objects = Hashtable.newFrom({"MUSIQUES": gdjs.FICHIER07Code.GDMUSIQUESObjects1});gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDMONDEObjects1Objects = Hashtable.newFrom({"MONDE": gdjs.FICHIER07Code.GDMONDEObjects1});gdjs.FICHIER07Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.FICHIER07Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.FICHIER07Code.condition0IsTrue_0.val = false;
gdjs.FICHIER07Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER07Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER07Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER07Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER07Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER01", true);
}}

}


{

gdjs.FICHIER07Code.GDIMAGESObjects1.createFrom(runtimeScene.getObjects("IMAGES"));

gdjs.FICHIER07Code.condition0IsTrue_0.val = false;
gdjs.FICHIER07Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER07Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER07Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER07Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDIMAGESObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER07Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER09", true);
}}

}


{

gdjs.FICHIER07Code.GDMUSIQUESObjects1.createFrom(runtimeScene.getObjects("MUSIQUES"));

gdjs.FICHIER07Code.condition0IsTrue_0.val = false;
gdjs.FICHIER07Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER07Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER07Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER07Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDMUSIQUESObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER07Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER12", true);
}}

}


{

gdjs.FICHIER07Code.GDMONDEObjects1.createFrom(runtimeScene.getObjects("MONDE"));

gdjs.FICHIER07Code.condition0IsTrue_0.val = false;
gdjs.FICHIER07Code.condition1IsTrue_0.val = false;
{
gdjs.FICHIER07Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.FICHIER07Code.condition0IsTrue_0.val ) {
{
gdjs.FICHIER07Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.FICHIER07Code.mapOfGDgdjs_46FICHIER07Code_46GDMONDEObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.FICHIER07Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "FICHIER08", true);
}}

}


{


{
}

}


}; //End of gdjs.FICHIER07Code.eventsList0x5b7028


gdjs.FICHIER07Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.FICHIER07Code.GDNewObjectObjects1.length = 0;
gdjs.FICHIER07Code.GDNewObjectObjects2.length = 0;
gdjs.FICHIER07Code.GDRETOURObjects1.length = 0;
gdjs.FICHIER07Code.GDRETOURObjects2.length = 0;
gdjs.FICHIER07Code.GDMONDEObjects1.length = 0;
gdjs.FICHIER07Code.GDMONDEObjects2.length = 0;
gdjs.FICHIER07Code.GDMUSIQUESObjects1.length = 0;
gdjs.FICHIER07Code.GDMUSIQUESObjects2.length = 0;
gdjs.FICHIER07Code.GDIMAGESObjects1.length = 0;
gdjs.FICHIER07Code.GDIMAGESObjects2.length = 0;

gdjs.FICHIER07Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['FICHIER07Code'] = gdjs.FICHIER07Code;
