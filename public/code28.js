gdjs.CM02Code = {};
gdjs.CM02Code.GDE3Objects1= [];
gdjs.CM02Code.GDE3Objects2= [];
gdjs.CM02Code.GDLObjects1= [];
gdjs.CM02Code.GDLObjects2= [];
gdjs.CM02Code.GDE2Objects1= [];
gdjs.CM02Code.GDE2Objects2= [];
gdjs.CM02Code.GDCObjects1= [];
gdjs.CM02Code.GDCObjects2= [];
gdjs.CM02Code.GDTObjects1= [];
gdjs.CM02Code.GDTObjects2= [];
gdjs.CM02Code.GDRObjects1= [];
gdjs.CM02Code.GDRObjects2= [];
gdjs.CM02Code.GDOObjects1= [];
gdjs.CM02Code.GDOObjects2= [];
gdjs.CM02Code.GDNObjects1= [];
gdjs.CM02Code.GDNObjects2= [];
gdjs.CM02Code.GDIObjects1= [];
gdjs.CM02Code.GDIObjects2= [];
gdjs.CM02Code.GDQObjects1= [];
gdjs.CM02Code.GDQObjects2= [];
gdjs.CM02Code.GDUObjects1= [];
gdjs.CM02Code.GDUObjects2= [];
gdjs.CM02Code.GDE1Objects1= [];
gdjs.CM02Code.GDE1Objects2= [];
gdjs.CM02Code.GDNewObjectObjects1= [];
gdjs.CM02Code.GDNewObjectObjects2= [];
gdjs.CM02Code.GDNewObject2Objects1= [];
gdjs.CM02Code.GDNewObject2Objects2= [];
gdjs.CM02Code.GDASCIIObjects1= [];
gdjs.CM02Code.GDASCIIObjects2= [];
gdjs.CM02Code.GDRETOURObjects1= [];
gdjs.CM02Code.GDRETOURObjects2= [];
gdjs.CM02Code.GDNewObject3Objects1= [];
gdjs.CM02Code.GDNewObject3Objects2= [];

gdjs.CM02Code.conditionTrue_0 = {val:false};
gdjs.CM02Code.condition0IsTrue_0 = {val:false};
gdjs.CM02Code.condition1IsTrue_0 = {val:false};
gdjs.CM02Code.condition2IsTrue_0 = {val:false};
gdjs.CM02Code.condition3IsTrue_0 = {val:false};
gdjs.CM02Code.condition4IsTrue_0 = {val:false};
gdjs.CM02Code.condition5IsTrue_0 = {val:false};
gdjs.CM02Code.condition6IsTrue_0 = {val:false};
gdjs.CM02Code.condition7IsTrue_0 = {val:false};
gdjs.CM02Code.condition8IsTrue_0 = {val:false};
gdjs.CM02Code.condition9IsTrue_0 = {val:false};
gdjs.CM02Code.condition10IsTrue_0 = {val:false};
gdjs.CM02Code.condition11IsTrue_0 = {val:false};
gdjs.CM02Code.condition12IsTrue_0 = {val:false};
gdjs.CM02Code.conditionTrue_1 = {val:false};
gdjs.CM02Code.condition0IsTrue_1 = {val:false};
gdjs.CM02Code.condition1IsTrue_1 = {val:false};
gdjs.CM02Code.condition2IsTrue_1 = {val:false};
gdjs.CM02Code.condition3IsTrue_1 = {val:false};
gdjs.CM02Code.condition4IsTrue_1 = {val:false};
gdjs.CM02Code.condition5IsTrue_1 = {val:false};
gdjs.CM02Code.condition6IsTrue_1 = {val:false};
gdjs.CM02Code.condition7IsTrue_1 = {val:false};
gdjs.CM02Code.condition8IsTrue_1 = {val:false};
gdjs.CM02Code.condition9IsTrue_1 = {val:false};
gdjs.CM02Code.condition10IsTrue_1 = {val:false};
gdjs.CM02Code.condition11IsTrue_1 = {val:false};
gdjs.CM02Code.condition12IsTrue_1 = {val:false};


gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRETOURObjects1Objects = Hashtable.newFrom({"RETOUR": gdjs.CM02Code.GDRETOURObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE3Objects1Objects = Hashtable.newFrom({"E3": gdjs.CM02Code.GDE3Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDLObjects1Objects = Hashtable.newFrom({"L": gdjs.CM02Code.GDLObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE2Objects1Objects = Hashtable.newFrom({"E2": gdjs.CM02Code.GDE2Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDCObjects1Objects = Hashtable.newFrom({"C": gdjs.CM02Code.GDCObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDTObjects1Objects = Hashtable.newFrom({"T": gdjs.CM02Code.GDTObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRObjects1Objects = Hashtable.newFrom({"R": gdjs.CM02Code.GDRObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDOObjects1Objects = Hashtable.newFrom({"O": gdjs.CM02Code.GDOObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDNObjects1Objects = Hashtable.newFrom({"N": gdjs.CM02Code.GDNObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDIObjects1Objects = Hashtable.newFrom({"I": gdjs.CM02Code.GDIObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDQObjects1Objects = Hashtable.newFrom({"Q": gdjs.CM02Code.GDQObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDUObjects1Objects = Hashtable.newFrom({"U": gdjs.CM02Code.GDUObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE1Objects1Objects = Hashtable.newFrom({"E1": gdjs.CM02Code.GDE1Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE1Objects1Objects = Hashtable.newFrom({"E1": gdjs.CM02Code.GDE1Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE2Objects1Objects = Hashtable.newFrom({"E2": gdjs.CM02Code.GDE2Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE3Objects1Objects = Hashtable.newFrom({"E3": gdjs.CM02Code.GDE3Objects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDLObjects1Objects = Hashtable.newFrom({"L": gdjs.CM02Code.GDLObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDCObjects1Objects = Hashtable.newFrom({"C": gdjs.CM02Code.GDCObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDTObjects1Objects = Hashtable.newFrom({"T": gdjs.CM02Code.GDTObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRObjects1Objects = Hashtable.newFrom({"R": gdjs.CM02Code.GDRObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDOObjects1Objects = Hashtable.newFrom({"O": gdjs.CM02Code.GDOObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDNObjects1Objects = Hashtable.newFrom({"N": gdjs.CM02Code.GDNObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDIObjects1Objects = Hashtable.newFrom({"I": gdjs.CM02Code.GDIObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDQObjects1Objects = Hashtable.newFrom({"Q": gdjs.CM02Code.GDQObjects1});gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDUObjects1Objects = Hashtable.newFrom({"U": gdjs.CM02Code.GDUObjects1});gdjs.CM02Code.eventsList0x5b7028 = function(runtimeScene) {

{

gdjs.CM02Code.GDRETOURObjects1.createFrom(runtimeScene.getObjects("RETOUR"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRETOURObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "MAP", true);
}}

}


{

gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE3Objects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
/* Reuse gdjs.CM02Code.GDE3Objects1 */
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDLObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
/* Reuse gdjs.CM02Code.GDLObjects1 */
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE2Objects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
/* Reuse gdjs.CM02Code.GDE2Objects1 */
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDCObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDCObjects1 */
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDTObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
/* Reuse gdjs.CM02Code.GDTObjects1 */
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
/* Reuse gdjs.CM02Code.GDRObjects1 */
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDOObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
/* Reuse gdjs.CM02Code.GDOObjects1 */
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("208;2;273");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDNObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
/* Reuse gdjs.CM02Code.GDNObjects1 */
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDIObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
/* Reuse gdjs.CM02Code.GDIObjects1 */
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDQObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
/* Reuse gdjs.CM02Code.GDQObjects1 */
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDUObjects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
/* Reuse gdjs.CM02Code.GDUObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("208;2;27");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("226;240;217");
}
}}

}


{

gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE1Objects1Objects, runtimeScene, true, false);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
/* Reuse gdjs.CM02Code.GDE1Objects1 */
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setColor("226;240;217");
}
}{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setColor("208;2;27");
}
}}

}


{

gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "e");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "E");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE1Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDE1Objects1 */
{for(var i = 0, len = gdjs.CM02Code.GDE1Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE1Objects1[i].setString("E");
}
}}

}


{

gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "e");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "E");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE2Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDE2Objects1 */
{for(var i = 0, len = gdjs.CM02Code.GDE2Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE2Objects1[i].setString("E");
}
}}

}


{

gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "e");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "E");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDE3Objects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDE3Objects1 */
{for(var i = 0, len = gdjs.CM02Code.GDE3Objects1.length ;i < len;++i) {
    gdjs.CM02Code.GDE3Objects1[i].setString("E");
}
}}

}


{

gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "l");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "L");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDLObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDLObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDLObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDLObjects1[i].setString("L");
}
}}

}


{

gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "c");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "C");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDCObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDCObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDCObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDCObjects1[i].setString("C");
}
}}

}


{

gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "t");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "T");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDTObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDTObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDTObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDTObjects1[i].setString("T");
}
}}

}


{

gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "r");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "R");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDRObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDRObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDRObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDRObjects1[i].setString("R");
}
}}

}


{

gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "o");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "O");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDOObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDOObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDOObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDOObjects1[i].setString("O");
}
}}

}


{

gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "n");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "N");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDNObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDNObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDNObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDNObjects1[i].setString("N");
}
}}

}


{

gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "i");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "I");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDIObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDIObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setString("I");
}
}}

}


{

gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "q");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "Q");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDQObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDQObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDQObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDQObjects1[i].setString("Q");
}
}}

}


{

gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
{
{gdjs.CM02Code.conditionTrue_1 = gdjs.CM02Code.condition0IsTrue_0;
gdjs.CM02Code.condition0IsTrue_1.val = false;
gdjs.CM02Code.condition1IsTrue_1.val = false;
{
gdjs.CM02Code.condition0IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "u");
if( gdjs.CM02Code.condition0IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
gdjs.CM02Code.condition1IsTrue_1.val = gdjs.evtTools.input.wasKeyReleased(runtimeScene, "U");
if( gdjs.CM02Code.condition1IsTrue_1.val ) {
    gdjs.CM02Code.conditionTrue_1.val = true;
}
}
{
}
}
}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
gdjs.CM02Code.condition1IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.CM02Code.mapOfGDgdjs_46CM02Code_46GDUObjects1Objects, runtimeScene, true, false);
}}
if (gdjs.CM02Code.condition1IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDUObjects1 */
{for(var i = 0, len = gdjs.CM02Code.GDUObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDUObjects1[i].setString("U");
}
}}

}


{

gdjs.CM02Code.GDCObjects1.createFrom(runtimeScene.getObjects("C"));
gdjs.CM02Code.GDE1Objects1.createFrom(runtimeScene.getObjects("E1"));
gdjs.CM02Code.GDE2Objects1.createFrom(runtimeScene.getObjects("E2"));
gdjs.CM02Code.GDE3Objects1.createFrom(runtimeScene.getObjects("E3"));
gdjs.CM02Code.GDIObjects1.createFrom(runtimeScene.getObjects("I"));
gdjs.CM02Code.GDLObjects1.createFrom(runtimeScene.getObjects("L"));
gdjs.CM02Code.GDNObjects1.createFrom(runtimeScene.getObjects("N"));
gdjs.CM02Code.GDOObjects1.createFrom(runtimeScene.getObjects("O"));
gdjs.CM02Code.GDQObjects1.createFrom(runtimeScene.getObjects("Q"));
gdjs.CM02Code.GDRObjects1.createFrom(runtimeScene.getObjects("R"));
gdjs.CM02Code.GDTObjects1.createFrom(runtimeScene.getObjects("T"));
gdjs.CM02Code.GDUObjects1.createFrom(runtimeScene.getObjects("U"));

gdjs.CM02Code.condition0IsTrue_0.val = false;
gdjs.CM02Code.condition1IsTrue_0.val = false;
gdjs.CM02Code.condition2IsTrue_0.val = false;
gdjs.CM02Code.condition3IsTrue_0.val = false;
gdjs.CM02Code.condition4IsTrue_0.val = false;
gdjs.CM02Code.condition5IsTrue_0.val = false;
gdjs.CM02Code.condition6IsTrue_0.val = false;
gdjs.CM02Code.condition7IsTrue_0.val = false;
gdjs.CM02Code.condition8IsTrue_0.val = false;
gdjs.CM02Code.condition9IsTrue_0.val = false;
gdjs.CM02Code.condition10IsTrue_0.val = false;
gdjs.CM02Code.condition11IsTrue_0.val = false;
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDE3Objects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDE3Objects1[i].getString() == "E" ) {
        gdjs.CM02Code.condition0IsTrue_0.val = true;
        gdjs.CM02Code.GDE3Objects1[k] = gdjs.CM02Code.GDE3Objects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDE3Objects1.length = k;}if ( gdjs.CM02Code.condition0IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDLObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDLObjects1[i].getString() == "L" ) {
        gdjs.CM02Code.condition1IsTrue_0.val = true;
        gdjs.CM02Code.GDLObjects1[k] = gdjs.CM02Code.GDLObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDLObjects1.length = k;}if ( gdjs.CM02Code.condition1IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDE2Objects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDE2Objects1[i].getString() == "E" ) {
        gdjs.CM02Code.condition2IsTrue_0.val = true;
        gdjs.CM02Code.GDE2Objects1[k] = gdjs.CM02Code.GDE2Objects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDE2Objects1.length = k;}if ( gdjs.CM02Code.condition2IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDCObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDCObjects1[i].getString() == "C" ) {
        gdjs.CM02Code.condition3IsTrue_0.val = true;
        gdjs.CM02Code.GDCObjects1[k] = gdjs.CM02Code.GDCObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDCObjects1.length = k;}if ( gdjs.CM02Code.condition3IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDTObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDTObjects1[i].getString() == "T" ) {
        gdjs.CM02Code.condition4IsTrue_0.val = true;
        gdjs.CM02Code.GDTObjects1[k] = gdjs.CM02Code.GDTObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDTObjects1.length = k;}if ( gdjs.CM02Code.condition4IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDRObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDRObjects1[i].getString() == "R" ) {
        gdjs.CM02Code.condition5IsTrue_0.val = true;
        gdjs.CM02Code.GDRObjects1[k] = gdjs.CM02Code.GDRObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDRObjects1.length = k;}if ( gdjs.CM02Code.condition5IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDOObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDOObjects1[i].getString() == "O" ) {
        gdjs.CM02Code.condition6IsTrue_0.val = true;
        gdjs.CM02Code.GDOObjects1[k] = gdjs.CM02Code.GDOObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDOObjects1.length = k;}if ( gdjs.CM02Code.condition6IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDNObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDNObjects1[i].getString() == "N" ) {
        gdjs.CM02Code.condition7IsTrue_0.val = true;
        gdjs.CM02Code.GDNObjects1[k] = gdjs.CM02Code.GDNObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDNObjects1.length = k;}if ( gdjs.CM02Code.condition7IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDIObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDIObjects1[i].getString() == "I" ) {
        gdjs.CM02Code.condition8IsTrue_0.val = true;
        gdjs.CM02Code.GDIObjects1[k] = gdjs.CM02Code.GDIObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDIObjects1.length = k;}if ( gdjs.CM02Code.condition8IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDQObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDQObjects1[i].getString() == "Q" ) {
        gdjs.CM02Code.condition9IsTrue_0.val = true;
        gdjs.CM02Code.GDQObjects1[k] = gdjs.CM02Code.GDQObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDQObjects1.length = k;}if ( gdjs.CM02Code.condition9IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDUObjects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDUObjects1[i].getString() == "U" ) {
        gdjs.CM02Code.condition10IsTrue_0.val = true;
        gdjs.CM02Code.GDUObjects1[k] = gdjs.CM02Code.GDUObjects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDUObjects1.length = k;}if ( gdjs.CM02Code.condition10IsTrue_0.val ) {
{
for(var i = 0, k = 0, l = gdjs.CM02Code.GDE1Objects1.length;i<l;++i) {
    if ( gdjs.CM02Code.GDE1Objects1[i].getString() == "E" ) {
        gdjs.CM02Code.condition11IsTrue_0.val = true;
        gdjs.CM02Code.GDE1Objects1[k] = gdjs.CM02Code.GDE1Objects1[i];
        ++k;
    }
}
gdjs.CM02Code.GDE1Objects1.length = k;}}
}
}
}
}
}
}
}
}
}
}
if (gdjs.CM02Code.condition11IsTrue_0.val) {
/* Reuse gdjs.CM02Code.GDIObjects1 */
{gdjs.evtTools.sound.playMusic(runtimeScene, "SF-yehaw.mp3", false, 100, 1);
}{for(var i = 0, len = gdjs.CM02Code.GDIObjects1.length ;i < len;++i) {
    gdjs.CM02Code.GDIObjects1[i].setString("l");
}
}{gdjs.evtTools.runtimeScene.resetTimer(runtimeScene, "T1");
}{gdjs.evtTools.runtimeScene.unpauseTimer(runtimeScene, "T1");
}}

}


{


gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.pauseTimer(runtimeScene, "T1");
}}

}


{


gdjs.CM02Code.condition0IsTrue_0.val = false;
{
gdjs.CM02Code.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.timerElapsedTime(runtimeScene, 3, "T1");
}if (gdjs.CM02Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "CM03", false);
}}

}


}; //End of gdjs.CM02Code.eventsList0x5b7028


gdjs.CM02Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.CM02Code.GDE3Objects1.length = 0;
gdjs.CM02Code.GDE3Objects2.length = 0;
gdjs.CM02Code.GDLObjects1.length = 0;
gdjs.CM02Code.GDLObjects2.length = 0;
gdjs.CM02Code.GDE2Objects1.length = 0;
gdjs.CM02Code.GDE2Objects2.length = 0;
gdjs.CM02Code.GDCObjects1.length = 0;
gdjs.CM02Code.GDCObjects2.length = 0;
gdjs.CM02Code.GDTObjects1.length = 0;
gdjs.CM02Code.GDTObjects2.length = 0;
gdjs.CM02Code.GDRObjects1.length = 0;
gdjs.CM02Code.GDRObjects2.length = 0;
gdjs.CM02Code.GDOObjects1.length = 0;
gdjs.CM02Code.GDOObjects2.length = 0;
gdjs.CM02Code.GDNObjects1.length = 0;
gdjs.CM02Code.GDNObjects2.length = 0;
gdjs.CM02Code.GDIObjects1.length = 0;
gdjs.CM02Code.GDIObjects2.length = 0;
gdjs.CM02Code.GDQObjects1.length = 0;
gdjs.CM02Code.GDQObjects2.length = 0;
gdjs.CM02Code.GDUObjects1.length = 0;
gdjs.CM02Code.GDUObjects2.length = 0;
gdjs.CM02Code.GDE1Objects1.length = 0;
gdjs.CM02Code.GDE1Objects2.length = 0;
gdjs.CM02Code.GDNewObjectObjects1.length = 0;
gdjs.CM02Code.GDNewObjectObjects2.length = 0;
gdjs.CM02Code.GDNewObject2Objects1.length = 0;
gdjs.CM02Code.GDNewObject2Objects2.length = 0;
gdjs.CM02Code.GDASCIIObjects1.length = 0;
gdjs.CM02Code.GDASCIIObjects2.length = 0;
gdjs.CM02Code.GDRETOURObjects1.length = 0;
gdjs.CM02Code.GDRETOURObjects2.length = 0;
gdjs.CM02Code.GDNewObject3Objects1.length = 0;
gdjs.CM02Code.GDNewObject3Objects2.length = 0;

gdjs.CM02Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['CM02Code'] = gdjs.CM02Code;
