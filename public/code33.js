gdjs.SORTIE1Code = {};
gdjs.SORTIE1Code.GDNewObjectObjects1= [];
gdjs.SORTIE1Code.GDNewObjectObjects2= [];

gdjs.SORTIE1Code.conditionTrue_0 = {val:false};
gdjs.SORTIE1Code.condition0IsTrue_0 = {val:false};
gdjs.SORTIE1Code.condition1IsTrue_0 = {val:false};


gdjs.SORTIE1Code.eventsList0x5b7028 = function(runtimeScene) {

{


gdjs.SORTIE1Code.condition0IsTrue_0.val = false;
{
gdjs.SORTIE1Code.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.SORTIE1Code.condition0IsTrue_0.val) {
{gdjs.evtTools.runtimeScene.replaceScene(runtimeScene, "SORTIE2", false);
}}

}


}; //End of gdjs.SORTIE1Code.eventsList0x5b7028


gdjs.SORTIE1Code.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.SORTIE1Code.GDNewObjectObjects1.length = 0;
gdjs.SORTIE1Code.GDNewObjectObjects2.length = 0;

gdjs.SORTIE1Code.eventsList0x5b7028(runtimeScene);
return;

}

gdjs['SORTIE1Code'] = gdjs.SORTIE1Code;
